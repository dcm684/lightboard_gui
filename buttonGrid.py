""" The main module for the button grid program.

There does not appear to a reson for this to be imported, but who knows?

Copyright: Delta Charlie Mike 2010
License: For use by the author, Christopher Meyer, only
Author: Christopher Meyer
"""

# A python script that will generate the arrays for the LED light board.

# Written by - Christopher Meyer

# This script can generate the code necessary to implement the given partterns
# for the LED lightboard.  It can also send the list of commands necessary to
# flash specific patterns directly to the board.

import buttGrid_GUI

from Tkinter import sys, Tk
import tkMessageBox
from buttGrid_Interface import MCUInterface
import buttGrid_Settings
import buttGrid_Filer

from optparse import OptionParser


# def bin(x):
#     """Convert a number to binary"""
#     return ''.join(x & (1 << i) and '1' or '0' for i in range(7, -1, -1))


class ButtonGrid(object):

    """
    Initialize the Button grid
    """
    def __init__(self):
        """The main loop the buttonGrid program

        Parses the command line input before drawing and populating the window
        and running the main loop.

        Args:
            self: This class.
        """

        # Initialize the microcontroller interface
        self.micro_interface = MCUInterface()

        settings = buttGrid_Settings.Settings()

        parser = OptionParser()
        parser.add_option("-f", "--file",
                          dest="fileName",
                          help="File to open",
                          metavar="FILE",
                          default='')
        parser.add_option("-n", "--noserial",
                          action="store_false",
                          dest="useSerial",
                          help="Disables communication over the serial port. \
                This will override --port.",
                          default=True)
        parser.add_option("-p", "--port",
                          action="store", type="string",
                          dest="serial_port",
                          help="Serial port connected to the microcontroller",
                          default=settings.serial_port_name)
        parser.add_option("-c", "--code",
                          action="store_true",
                          dest="auto_code",
                          help="Creates C code for the given file and exits."
                               " NoGUI",
                          default=False)
        parser.add_option("-u", "--upload",
                          action="store_true",
                          dest="auto_upload",
                          help="Uploads the given file and exits. NoGUI",
                          default=False)

        (options, args) = parser.parse_args()

        # If the user wants serial port control, set it up
        serial_success = True
        error_string = ""
        if settings.serial_port_name is None:
            if options.serial_port is not None:
                serial_success, error_string = \
                    self.micro_interface.set_serial_port(options.serial_port)
        else:
                serial_success, error_string = \
                    self.micro_interface.set_serial_port(
                        settings.serial_port_name)

        # Exit if on a non-GUI mode that uses serial and a serial
        # error occurred
        if not serial_success and options.auto_upload:
            print error_string
            exit()

        # Determine the name of the file to open if it exists
        if options.fileName != '':
            file_to_open = options.fileName
        elif len(args) > 0:
            file_to_open = args[0]
        else:
            file_to_open = None

        # If a non-GUI option was given, use it now then exit
        if options.auto_upload or options.auto_code:
            if file_to_open is None:
                print("No file given to open")
                exit()

            # Attempt to open the file
            board_layout = buttGrid_Filer.FileManager.load_file(file_to_open)

            if board_layout is None:
                print("File did not contain a valid board layout")
                exit()

            if options.auto_upload:
                # Attempt to send the pattern to the microcontroller
                success, error_string = self.micro_interface.\
                    export_to_serial(board_layout)

                if success:
                    print("Successfully sent %s" % file_to_open)
                else:
                    print("Failed to send %s\r\n%s" %
                          (file_to_open, error_string))

            else:
                # Export the C code
                self.micro_interface.export_to_code(board_layout)

            exit()

        # Start the GUI up
        root = Tk()

        root.option_add("*Font", "helvetica 12")

        if not serial_success:
            tkMessageBox.showerror("Serial Port Error",
                                   error_string)

        # Set up the main window of the program
        self.main_frame = buttGrid_GUI.MainFrame(
            root=root,
            settings=settings,
            use_serial=options.useSerial,
            micro_interface=self.micro_interface)

        root.protocol("WM_DELETE_WINDOW", self.exit_handler)

        # If a file was given at the command prompt open it
        if file_to_open is not None:
            self.main_frame.menu_bar.open_this_file(file_to_open)

        try:
            root.mainloop()
        except SystemExit:
            self.exit_handler()

    """Top level class of the buttonGrid program.

    This class contains the functions used to start and exit the program

    Attributes:
        my_menu_bar: The instance of the menu class
    """
    def exit_handler(self):
        """Prompts the user to save before exiting the program

        Args:
              self: This class.
        """
        if self.main_frame.menu_bar.prompt_user_to_save():
            self.main_frame.menu_bar.noChangesAfterSave = True
            sys.exit()

# Runs the program if this script is not imported
if __name__ == '__main__':
    ButtonGrid()
