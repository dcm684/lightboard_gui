"""
This module contains the functionality used to communicate with
the microcontroller

Copyright: Delta Charlie Mike 2010
License: For use by the author, Christopher Meyer, only
Author: Christopher Meyer
"""
import serial

import time


class MCUInterface(object):
    """Consists of the functions that  are used to get the display from the
    computer to the microcontroller

    Attributes:
        SEND_SERIAL_CHAR: Character sent to the microcontroller indicating
            that a display pattern is about to be sent.
        CHECKSUM_CHAR: Character received from the microcontroller indicating
            that the next character is a checksum byte.
        COMPLETE_CHAR:  Character sent to the microcontroller indicating that
            there are no further transmissions.
        NV_CLEAR_CHAR: Character sent to the microcontroller telling it to
            clear the pattern in the non-volatile memory and revert to the
            default value

        SERIAL_TIME_OUT: Number of 10ms periods of silence from the
            microcontroller that must pass before communication is terminated
            prematurely.
        DEFAULT_SERIAL_PORT: If no serial port is specified in the
            command line this serial port will be used.
        BAUD_RATE: Baud rate to use for communication

        serial_open: bool indicating whether there is an open
                        serial connection
        mySerial: The serial port connection used to communicate
                        with the board

    """
    SEND_SERIAL_CHAR = 'S'
    CHECKSUM_CHAR = 'X'
    COMPLETE_CHAR = 'T'

    NV_CLEAR_CHAR = 'C'
    SERIAL_TIME_OUT = 1.0  # Time in seconds before timing out on a read
    DEFAULT_SERIAL_PORT = 'COM5'
    BAUD_RATE = 115200

    def __init__(self):
        self.serial_open = False
        self.mySerial = None

    def export_to_code(self, screen_array):
        """Prints the code to the console that can be copied to the
        WinAVR microcontroller source code in order to display the open
        sequence of patterns."""

        import buttGrid_Layout
        assert isinstance(screen_array, buttGrid_Layout.BoardLayout)

        print '\r\nuint8_t outputArrayLength = %s;' % (screen_array.length())

        print '\r\nuint8_t outputArray[MAX_LENGTH][13] = {'

        delay_string = 'uint8_t delayArray[MAX_LENGTH] = { '
        for aScreen in range(screen_array.length()):
            button_states, delay = screen_array.get_screen(aScreen)
            # print '(', button_states[10], ')'
            delay_string += str(delay)

            button_states = self.convert_screen_for_export(button_states)
            print '\t{',
            for index in range(13):
                row_byte = button_states[index]
                if index < 12:
                    print '0x%X,' % row_byte,
                else:
                    if aScreen < (screen_array.length() - 1):
                        print '0x%X },' % row_byte
                    else:
                        print '0x%X }' % row_byte

            if aScreen < (screen_array.length() - 1):
                delay_string += ', '
            else:
                delay_string += ' };'
        print '};'

        print '\r\n', delay_string

    def export_to_serial(self, screen_array):
        """Sends the pattern to the lightboard

        The process for sending the sending the sequence of patterns is:
        #. Send two COMPLETE_CHAR.
        #. Send one SEND_SERIAL_CHAR.
        #. Send the MCU the number of screens that will be sent.
        #. Wait for the MCU to respond with the value it received.
            0 will be received if the number of screens received is invalid
        #. Send the first screen to the MCU.

            * The 10x10 bit arrays shall be compacted into 13 8-bit
                arrays using convert_screen_for_export.
            * The delay
            * Checksum of pattern and delay bytes XORd

        #. Wait for CHECKSUM_CHAR followed by a checksum value from the MCU.

            * The checksum is a byte value of all of the bytes in a
                screen XORed including the checksum. 0 will be received
                in the case of a valid transmit

        #. Repeat the previous two steps for all of the remaining screens..

        #. Serial communication is complete.

        :param screen_array: List of screens to be sent

        :return A tuple
                Whether the entire communication was successful
                String with the error. It will be empty if there was no error
        """

        error_string = "Export Failed\r\n\n"

        # Open the serial connection
        connect_success, received_error_string = self.connect_to_serial_port()
        if not connect_success:
            return False, error_string + received_error_string

        import buttGrid_Layout
        assert isinstance(screen_array, buttGrid_Layout.BoardLayout)

        print "Exporting over serial port ", self.mySerial.port

        self.mySerial.flushOutput()
        self.mySerial.flushInput()

        # Tell microcontroller that data is coming
        self.mySerial.write(self.COMPLETE_CHAR +
                            self.COMPLETE_CHAR +
                            self.SEND_SERIAL_CHAR)

        # Wait for the control board to respond, letting this program know
        # that it is ready for communication.
        successful_read = False
        start_time = time.time()
        while time.time() - start_time < self.SERIAL_TIME_OUT:
            if self.mySerial.inWaiting() > 0:
                if self.SEND_SERIAL_CHAR == self.mySerial.read():
                    successful_read = True
                    break

        if not successful_read:
            error_string += "Sequence send request response never received"
            self.close_serial_port()
            return False, error_string

        # Get the maximum number of screens supported by the display
        start_time = time.time()
        while time.time() - start_time < self.SERIAL_TIME_OUT:
            if self.mySerial.inWaiting() > 1:
                max_board_len = (ord(self.mySerial.read(1)) << 8) +\
                                ord(self.mySerial.read(1))
                print "Max board len ", max_board_len
                if max_board_len < screen_array.length():
                    print "Board max length (%u) < Screen count (%u)" % (
                        max_board_len,
                        screen_array.length()
                    )
                break

        expected_length = screen_array.length()
        read_length = expected_length + 1  # Init to a wrong value

        print "\r\nTrue Length "
        print expected_length

        # Send the board the new screen count. Cannot use a terminate
        # char since the first byte of a screen might be that char
        self.mySerial.write("" + chr(expected_length))

        successful_read = False
        start_time = time.time()

        while time.time() - start_time < self.SERIAL_TIME_OUT:
            if self.mySerial.inWaiting() > 0:
                read_length = ord(self.mySerial.read())
                if read_length == expected_length:
                    successful_read = True
                    print "\r\nValid Length\r\n"
                else:
                    error_string += "Invalid Pattern Length: %u\r\n" % \
                                    read_length
                    print "\r\n%s" % error_string
                break

        if not successful_read:
            error_string += "Failed on sending length"
            self.close_serial_port()
            return False, error_string

        # @TODO: Exit function immediately after a timeout or failure
        for screen_number in range(screen_array.length()):
            button_states, delay = screen_array.get_screen(screen_number)

            checksum = 0
            # Convert the screens into a byte list and tack the delay on
            out_screen = self.convert_screen_for_export(button_states)
            out_screen.append(delay)

            print '%3u / %3u:\t' % (screen_number, screen_array.length()),

            # Send the screen
            for anIndex in range(len(out_screen)):
                checksum = checksum ^ out_screen[anIndex]
                self.mySerial.write(chr(out_screen[anIndex]))

                print "%02X " % (out_screen[anIndex]),

            # Send the checksum
            self.mySerial.write(chr(checksum))

            print '(%02X)\r\n' % checksum,

            # Wait for the response checksum which should be 0
            valid_checksum = False
            start_time = time.time()
            while time.time() - start_time < self.SERIAL_TIME_OUT:
                while self.mySerial.inWaiting() > 0:
                    read_checksum = ord(self.mySerial.read(1))

                    if read_checksum == 0:
                        valid_checksum = True
                        break

                    else:
                        print "Received wrong CS: %2X" % read_checksum

                if valid_checksum:
                    break

            if not valid_checksum:
                error_string += \
                    "Invalid checksum received for screen %u" % screen_number
                break

        # Close the serial connection
        self.close_serial_port()

        # If transmit was successful, clear the error string
        if valid_checksum:
            error_string = ""

        print "\r\nDONE"

        return valid_checksum, error_string

    def connect_to_serial_port(self, port_name=None):
        """Creates the connection to the serial port.

        Args:
            port_name: The full name of the serial port, e.g. COM2, /dev/tty0
                        If None is given default to the current port in
                        self.mySerial

        Returns:
            A tuple with:
                0: A boolean indicating the success of the connection
                1: Error string if failed otherwise an empty string
        """
        success = True
        error_string = ""

        # Default port is the one in self.mySerial.port
        if port_name is None:
            if self.mySerial is not None:
                port_name = self.mySerial.port

        if port_name is not None and len(port_name) > 0:
            try:
                self.mySerial = serial.Serial(port_name,
                                              self.BAUD_RATE,
                                              timeout=1)

                self.serial_open = True
            except serial.SerialException:
                error_string = "Could not gain access to %s. " \
                               "Is it a valid port?" % port_name
                success = False
            except ValueError:
                error_string = "Baud Rate , %u bps, is incompatible " \
                               "with this port." % self.BAUD_RATE
                success = False
        else:
            error_string = "No port is configured"
            success = False

        print error_string

        return success, error_string

    def set_serial_port(self, port_name):
        """ Sets the communication port to the one given

        Port will be tested by connecting to it and then immedately
        closing the connection

        :param port_name: Serial port used to communicate with the
        microcontroller

        :return: A tuple
                    True, if the port was valid
                    A string describing why the port was invalid otherwise
                    an empty string
        """

        valid_port = False
        error_string = ""

        if not self.serial_open:
            valid_port, error_string = self.connect_to_serial_port(port_name)
            if valid_port:
                try:
                    self.mySerial.port = port_name
                except ValueError:
                    error_string = "Problem connecting to serial port, %s. " \
                                   "Try again later" % port_name

            self.close_serial_port()

        else:
            error_string = "Serial port was already open: ", port_name

        return valid_port, error_string

    def close_serial_port(self):
        """ Closes the active serial port

        Returns:
            Port that was closed. If this function is unsuccessful or
            no port is active, None will be returned
        """

        if self.serial_open:
            try:
                active_port = self.mySerial.port
                self.mySerial.close()
            except serial.SerialException:
                active_port = None

        else:
            active_port = None

        self.serial_open = False

        return active_port

    def serial_send_a_command(self, out_char):
        """Sends a char over the serial port and returns whether
        a confirmation was received

        The port must be open for a successful send

        Args:
            out_char: The character being sent

        Returns:
            A boolean indicating the success of the connection
        """

        confirm_received = False

        self.mySerial.write(out_char)

        start_time = time.time()
        while time.time() - start_time < self.SERIAL_TIME_OUT:
            if self.mySerial.inWaiting() > 0:
                if self.mySerial.read(1) == out_char:
                    confirm_received = True
                    break

        return confirm_received

    def serial_clear_nv_memory(self):
        """Clears the non-volatile memory on the board.

        Returns:A tuple
                    True, if the data was cleared successfully in NV Memory
                    A string explaining why it failed to clear,
                    otherwise an empty string
        """

        clear_succeeded = False

        # Open the serial connection
        clear_succeeded, error_string = self.connect_to_serial_port()
        if clear_succeeded:
            clear_succeeded = self.serial_send_a_command(self.NV_CLEAR_CHAR)

        # Close the port
        self.close_serial_port()

        if clear_succeeded:
            print 'NV Memory Clear Success'
        else:
            error_string = "NV Memory Clear Command Fail\r\n\n" + error_string
            print error_string

        return clear_succeeded, error_string

    def convert_screen_for_export(cls, button_states):
        """Converts a 10x10 array to a 13x8 array.

        The structure of the returned array is represented below. Each bit
          from a byte is represented with the value of its key in the 10 by
          10-bit array. For example all bits in byte 0 are represented by
          a 0.

          0. 00000000
          1. 11111100
          2. 22221111
          3. 33222222
          4. 33333333
          5. 44444444
          6. 55555544
          7. 66665555
          8. 77666666
          9. 77777777
          A. 88888888
          B. 99999988
          C. XXXX9999 (4MSB are ignored by MCU)

        Args:
            button_states: An array of button states for one screen

        Returns:
            A 13 by 8-bit array of the screen
        """
        screen_bytes = []
        out_byte = 0
        for anIndex in range(13):
            if anIndex > 9:
                b1 = 8
                b2 = 8
            elif anIndex > 4:
                b1 = 4
                b2 = 4
            else:
                b1 = 0
                b2 = 0

            if (anIndex == 0) or (anIndex == 5) or (anIndex == 10):
                out_byte = button_states[b2]

            elif (anIndex == 1) or (anIndex == 6) or (anIndex == 11):
                b2 += 1
                out_byte = (button_states[b2] << 2) | (button_states[b1] >> 8)

            elif (anIndex == 2) or (anIndex == 7) or (anIndex == 12):
                b1 += 1
                b2 += 2
                if anIndex == 12:
                    out_byte = button_states[b1] >> 6
                else:
                    out_byte = (button_states[b2] << 4) | \
                               (button_states[b1] >> 6)

            elif (anIndex == 3) or (anIndex == 8):
                b1 += 2
                b2 += 3
                out_byte = (button_states[b2] << 6) | (button_states[b1] >> 4)

            elif (anIndex == 4) or (anIndex == 9):
                b1 += 3
                out_byte = button_states[b1] >> 2

            screen_bytes.append(out_byte & 0xFF)
        return screen_bytes
