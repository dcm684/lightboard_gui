__author__ = 'Meyer'

import pickle
import buttGrid_Layout

class FileManager(object):
    """
    Reads and write layout save files
    """

    def __init__(self):
        pass

    @staticmethod
    def load_file(file_path):
        """ Loads the given pattern save file

        :param file_path: Path to the save file
        :return: The screen array stored in the file if it exists, None
        otherwise

        Raises:
            IOError: An error occurred when opening or closing the file.
        """

        # Attempt to open the file
        try:
            f = open(file_path, 'r')
            temp = pickle.load(f)
            f.close()
        except IOError:
            print('File "%s" does not exist.' % file_path)
            return None

        board_layout = buttGrid_Layout.BoardLayout()

        # Load the value from the list in the save file
        for screen in temp:

            # Import old style list(11) or new style
            # tuple(pattern, delay) saves
            if isinstance(screen, tuple):
                pattern, delay = screen
            elif isinstance(screen, list) and len(screen) == 11:
                pattern = screen[0:10]
                delay = int(screen[10])
            else:
                print "Error in file"
                print screen
                return None

            board_layout.append_screen(pattern, delay)

        return board_layout
