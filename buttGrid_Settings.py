"""Handles the settings menu and making them persistent

Copyright: Delta Charlie Mike 2011
License: For use by the author, Christopher Meyer, only
Author: Christopher Meyer
"""
import Tkinter
import serial_scan
import ConfigParser

import buttGrid_Interface

from Tkinter import Toplevel, TclError
import tkMessageBox


class SettingsMenu(object):
    """Provides a means of making persistent settings

    Attributes:
        root: Root window
        mcu_interface: Object used to interface with the microcontroller
        def_delay_intvar: String variable containing the default delay value
        port_list: List of available serial ports
        port_listbox: Listbox containing available serial ports
    """

    def __init__(self, root, mcu_interface, settings=None, master=None):
        """ Initializes the settings menu then hides

        :param root: Root window to display on
        :param mcu_interface: Interface with the microcontroller
        :param settings: Settings object to use and store
        :param master: Unused
        :return:
        """

        self.root = root

        assert isinstance(mcu_interface, buttGrid_Interface.MCUInterface)
        self.mcu_interface = mcu_interface

        # Default some default values for attributes

        self.port_list = []
        self.port_listbox = Tkinter.Listbox()

        # Use the provided settings object if it exists otherwise create a
        # new one. Read from the settings file regardless of whether one
        # was provided or not to ensure values are up-to-date
        if settings is None or not isinstance(settings, Settings):
            self.settings = Settings()
        else:
            self.settings = settings

        self.settings.read_settings()

        self.def_delay_intvar = Tkinter.IntVar(
            value=self.settings.default_delay)

        self.this_window = self.create_settings_window()

        self.this_window.withdraw()

    def show_settings(self):
        """Unhides the settings window"""
        try:
            # Update what is to be shown in the window
            self.update_port_list(True)
            self.def_delay_intvar.set(self.settings.default_delay)
            self.set_serial_port_listbox(self.settings.serial_port_name)

            self.this_window.deiconify()
        except TclError:
            # The window was closed and not exited via the OK or Cancel buttons
            # It needs to be recreated.
            self.create_settings_window()

    def update_port_list(self, rescan=True):
        """Rescans the available serial ports and updates the list displaying
        them

        :param rescan: Rescan the list or use the existing ports in port_list.
                        Will always rescan if port_list is empty

        :return:
        """
        if rescan or len(self.port_list) == 0:
            # Create a list of all serial ports
            self.port_list = serial_scan.scan()

        # Clear the listbox
        self.port_listbox.delete(0, Tkinter.END)

        # Add items to the port list
        if len(self.port_list) > 0:
            for aPort in range(len(self.port_list)):
                self.port_list[aPort] = self.port_list[aPort][1]
                self.port_listbox.insert(Tkinter.END, self.port_list[aPort])
        else:
            self.port_list.append("None")

        if len(self.port_list) == 0:
            self.port_listbox.config(state=Tkinter.DISABLED)

    def create_settings_window(self):
        """Creates the settings window, populates it with the settings widgets
            and populates those widgets with their values
        """
        this_window = Toplevel(self.root)
        this_window.title("Settings")

        Tkinter.Label(this_window,
                      text="Serial Port").grid(row=0,
                                               column=0,
                                               padx=2)

        # Create a list of all serial ports
        self.port_list = serial_scan.scan()

        # Create and add the listbox to display the ports
        port_list_scroller = Tkinter.Scrollbar(this_window,
                                               orient=Tkinter.VERTICAL)
        port_list_scroller.grid(row=0,
                                column=2,
                                sticky=Tkinter.N+Tkinter.S)

        port_list_height = 4
        if len(self.port_list) < port_list_height:
            port_list_height = len(self.port_list)

        self.port_listbox = Tkinter.Listbox(this_window,
                                            activestyle="none",
                                            selectmode=Tkinter.BROWSE,
                                            height=port_list_height,
                                            width=10,
                                            exportselection=0,  # Prevent
                                            # losing the selection when
                                            # selecting text in an entry
                                            yscrollcommand=port_list_scroller.
                                            set)

        port_list_scroller["command"] = self.port_listbox.yview

        self.update_port_list(False)

        self.set_serial_port_listbox(self.settings.serial_port_name)

        self.port_listbox.grid(row=0,
                               column=1)

        # Add a default delay entry
        Tkinter.Label(this_window,
                      text="Default Delay").grid(row=1,
                                                 column=0,
                                                 padx=2)

        default_delay_entry = Tkinter.Entry(this_window,
                                            width=4,
                                            textvariable=self.def_delay_intvar
                                            )

        default_delay_entry.grid(row=1,
                                 column=1)

        # Add the OK and Cancel buttons
        def save_and_exit():
            if self.save_settings():
                self.this_window.destroy()
            else:
                self.this_window.lift()

        Tkinter.Button(this_window,
                       text="OK",
                       command=save_and_exit).grid(row=3,
                                                   column=0)
        Tkinter.Button(this_window,
                       text="Cancel",
                       command=this_window.destroy).grid(row=3,
                                                         column=1)

        return this_window

    def save_settings(self):
        """Applies the settings to the variables and has them stored in the
            config file

            If there is an invalid configuration, false will be reutrned and
            nothing will be saved

            Returns:
                True, if the options selected were valid
        """

        success = False

        if len(self.port_list) > 0 and self.port_list[0] != 'None':

            # Verify a port was selected
            selected_ports = map(int, self.port_listbox.curselection())
            if isinstance(selected_ports, list) and len(selected_ports) == 1:
                self.settings.serial_port_name = self.port_list[
                    int(selected_ports[0])]

                # If a port was selected, see if it is available
                success, error_string = self.mcu_interface.set_serial_port(
                    self.settings.serial_port_name)

                if not success:
                    tkMessageBox.showerror("Port Error",
                                           error_string)

            else:
                tkMessageBox.showerror("Port Error",
                                       "Please select one serial port")

        if success:
            self.settings.default_delay = self.def_delay_intvar.get()
            self.settings.write_settings()

        return success

    def set_serial_port_listbox(self, in_serial_port):
        """Receives the local instance of active serial port

        Selects the serial port name in the listbox
        """

        self.settings.serial_port_name = in_serial_port

        # Select the active serial port, if there is one
        selected_port_index = 0
        if self.settings.serial_port_name is not None:
            try:
                selected_port_index = self.port_list.index(
                    self.settings.serial_port_name)
            except ValueError:
                selected_port_index = 0
                print "No Match for %s" % in_serial_port
        else:
            print "No port given"

        self.port_listbox.selection_set(selected_port_index)
        self.port_listbox.see(selected_port_index)


class Settings(object):

    """
    Attributes:
        CONFIG_FILE: Files that stores all of the configuration data
        def_delay_intvar: String variable that holds the default delay

        DEFAULT_DELAY: The default number of 10ms periods keep a
            screen displayed

        default_delay: Default 10ms periods to hold on a screen
        serial_port_name: Serial port used to communicate with the
                            microcontroller
    """

    CONFIG_FILE = "settings.cfg"
    DEFAULT_DELAY = 20

    def __init__(self):
        """
        Initializes the object then reads the config data in

        :return: None
        """
        self.serial_port_name = ""
        self.default_delay = self.DEFAULT_DELAY

        self.read_settings()

    def write_settings(self):
        """ Writes the settings to a file

        :return: None
        """
        config = ConfigParser.RawConfigParser()

        # Prepare the data to be written
        if not config.has_section('Serial'):
            config.add_section('Serial')

        config.set('Serial', 'port', self.serial_port_name)

        if not config.has_section('Pattern'):
            config.add_section('Pattern')

        config.set('Pattern', 'default_delay', self.default_delay)

        # Write the file
        with open(self.CONFIG_FILE, 'wb') as configFile:
            config.write(configFile)
            configFile.close()

    def read_settings(self):
        """ Reads the settings file

        :return:
        """
        config = ConfigParser.RawConfigParser()
        config.read(self.CONFIG_FILE)

        # Read in the serial port
        if config.has_option('Serial', 'port'):
            self.serial_port_name = config.get('Serial', 'port')
        else:
            self.serial_port_name = None

        # Read in the default delay
        if config.has_option('Pattern', 'default_delay', ):
            self.default_delay = int(config.get('Pattern', 'default_delay'))
        else:
            self.default_delay = self.DEFAULT_DELAY
