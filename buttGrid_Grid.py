""" This module contains the classes used for the display and storage of
the sequence data.

Copyright: Delta Charlie Mike 2010
License: For use by the author, Christopher Meyer, only
Author: Christopher Meyer
"""

import Tkinter

from Tkinter import Button, N, S, E, W, Entry

import buttGrid_Settings
import buttGrid_GUI
import buttGrid_Layout


class LayoutScroller(Tkinter.Frame):
    """The scrollbar containing buttons representing each of the
    created states.

    Attributes:
        BUTT_SCRN_W: The width of a screen selector button
        BUTT_SCRN_H: The height of a screen selector button
        BUTT_SCRN_PX: The horizontal padding for a screen selector button
        BUTT_SCRN_PY: The horizontal padding for a screenselector button
        ENTRY_DLY_W: Width in characters for a delay text box

        layouts: The array containing all of the screen patterns and
            their associated delays in the form of a StringVar
        selectedScreen: The currently selected screen
        screenButtons: An array of buttons used to select a particular
            screen
        delayTextBoxes: An array of text boxes tied to the layouts
            that display a screen's delay period
        remove_button: The button used to remove the current screen
    """

    SCREENS_DISPLAYED = 8
    BUTT_SCRN_W = 5
    BUTT_SCRN_H = 3
    BUTT_SCRN_PX = 0
    BUTT_SCRN_PY = 0
    ENTRY_DLY_W = 4

    SHIFT_UP = "up"
    SHIFT_DOWN = "down"
    SHIFT_LEFT = "left"
    SHIFT_RIGHT = "right"

    def __init__(self, in_button_grid, in_settings, master=None):
        """Creates the canvas and gives it a scroll bar.

        Seems a little bit convoluted to create a frame within a canvas
        within a frame, but that's what had to be done to get a scrolling
        window. It's like X-Zibit said, "Yo dawg, I heard you liked
        frames, so I put a frames within a canvas within a frame."

        Args:
            in_button_grid: The button grid that will display the selected
                screen
            in_settings: Object with the current settings for the program
            master: Used in Tkinter.Frame.__init__(master)
        """
        assert isinstance(in_button_grid, ButtonGrid)
        self.theButtGrid = in_button_grid

        assert isinstance(in_settings, buttGrid_Settings.Settings)
        self.settings = in_settings

        self.layouts = buttGrid_Layout.BoardLayout()
        self.selectedScreen = 0

        self.screenButtons = []
        self.delayTextBoxes = []
        self.delayIntVars = []

        # Declare here so they can be known at init, but they will
        # get overwritten
        self.main_menu_bar = None  # Really a buttGrid_GUI.MenuBar

        self.removeButton = None  # Really a button

        Tkinter.Frame.__init__(self, master)
        self.grid()

        # Create the scrolling canvas for the screen select buttons and
        # their delays
        self.scrollX = Tkinter.Scrollbar(self, orient=Tkinter.HORIZONTAL)
        self.scrollX.grid(row=2, column=0, sticky=E+W)

        self.scroll_canvas = Tkinter.Canvas(self,
                                            xscrollcommand=self.scrollX.set,
                                            height=120,
                                            width=0)
        self.scroll_canvas.grid(row=0, column=0, sticky=N+S+E+W)

        self.scrollX.config(command=self.scroll_canvas.xview)

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.canvas_frame = Tkinter.Frame(self.scroll_canvas)
        self.canvas_frame.rowconfigure(1, weight=1)
        self.canvas_frame.columnconfigure(1, weight=1)

        # Create the select button for screen one
        select_screen_call = self._layout_button_handler(0)
        self.layouts.append_screen(self.layouts.BLANK_DISPLAY,
                                   self.settings.default_delay)

        self.screenButtons.append(Button(self.canvas_frame,
                                         width=self.BUTT_SCRN_W,
                                         height=self.BUTT_SCRN_H,
                                         padx=self.BUTT_SCRN_PX,
                                         pady=self.BUTT_SCRN_PY,
                                         text="0",
                                         command=select_screen_call,
                                         state=Tkinter.DISABLED))

        self.screenButtons[0].grid(row=1, column=0)

        # Create the delay text box for screen one
        self.delayIntVars.append(Tkinter.IntVar(
            value=self.settings.default_delay)
        )
        self.delayTextBoxes.append(Entry(self.canvas_frame,
                                         width=self.ENTRY_DLY_W,
                                         textvariable=self.delayIntVars[0]
                                         )
                                   )
        self.delayTextBoxes[0].grid(row=2, column=0, sticky=N+S)
        self.delayTextBoxes[0].focus_force()

        self.scroll_canvas.create_window(0, 0,
                                         anchor="nw",
                                         window=self.canvas_frame)

        self.canvas_frame.update_idletasks()
        self.scroll_canvas.config(scrollregion=self.scroll_canvas.bbox("all"))

    def set_menu_bar(self, in_main_menu_bar):
        """ Sets the reference for the local menu bar to one given.

        Args:
          in_main_menu_bar: The menu bar to reference.
        """
        assert isinstance(in_main_menu_bar, buttGrid_GUI.MenuBar)
        self.main_menu_bar = in_main_menu_bar

    def _layout_button_handler(self, calling_screen_select_button):
        """Selects the screen whose button was clicked.

        Args:
            calling_screen_select_button: The number of the screen
            represented by the button that called this function.
        """

        def handle_it():
            self.select_screen(calling_screen_select_button)
        return handle_it

    def select_screen(self, screen_number, previous_was_deleted=False):
        """Selects and displays the given screen.

        Args:
            screen_number: The number of the screen to display.

            previous_was_deleted: This function was called when a neighboring
            screen was deleted.
        """
        if self.layouts.validate_index(screen_number):

            # If the previous screen was not deleted, store the current
            # screen's pattern and delay and enable its select button
            if not previous_was_deleted:
                self.store_current_screen()

                self.screenButtons[self.selectedScreen].config(
                    state=Tkinter.NORMAL)

            # Select and show the new screen
            self.selectedScreen = screen_number
            self.screenButtons[screen_number].config(state=Tkinter.DISABLED)
            self.theButtGrid.set_button_states(
                self.layouts.get_pattern(self.selectedScreen))
            self.delayTextBoxes[screen_number].focus_force()

            # Follow the active screen in the scroll box if there are too many
            # screens to display all at once.
            screen_arr_len = self.layouts.length()
            if self.SCREENS_DISPLAYED < screen_arr_len:
                screens_shown = \
                    float(self.SCREENS_DISPLAYED) / screen_arr_len
                screens_left_of_center = \
                    float(screen_number + 1) / screen_arr_len - \
                    screens_shown / 2

                # Handle the cases of the screen being at the beginning or the
                # end of the pattern.
                if screen_number + 1 <= float(self.SCREENS_DISPLAYED) / 2:
                    screens_left_of_center = 0

                elif screen_number >= screen_arr_len - \
                        float(self.SCREENS_DISPLAYED) / 2:
                    screens_left_of_center = 1 - screens_shown

                self.scroll_canvas.xview_moveto(screens_left_of_center)

    def insert_new_screen(self,
                          place_to_insert=-1,
                          pattern=None,
                          delay=None,
                          focus_on_new=True):
        """Creates a new blank screen right after the current screen and
        then selects it

        Args:
            place_to_insert: Where to insert the new screen at. Default
                position is the next position in the sequence. If the
                received value is greater than the length, the new screen
                will be added on to the end of the sequence.

            pattern: Pattern to use for this new screen. Defaults to a blank
                    pattern

            delay: Delay to use for this new screen. Defaults to default value
                    in settings

            focus_on_new: Move focus to this, just added view. Defaults
                to True which would be expected. False is used during the
                initial load

        """
        if place_to_insert < 0:
            place_to_insert = self.selectedScreen + 1
        elif place_to_insert > self.layouts.length():
            place_to_insert = self.layouts.length()

        assert isinstance(self.main_menu_bar, buttGrid_GUI.MenuBar)
        self.main_menu_bar.noChangesAfterSave = False

        # If no pattern is given default to a blank display
        if pattern is None:
            pattern = self.layouts.BLANK_DISPLAY

        # If no delay is given use the default value
        if delay is None:
            delay = self.settings.default_delay

        self.layouts.insert_screen(pattern,
                                   delay,
                                   place_to_insert)

        # Create and add a button to select this button screen
        select_screen_call = self._layout_button_handler(place_to_insert)
        self.screenButtons.insert(place_to_insert,
                                  Button(self.canvas_frame,
                                         width=self.BUTT_SCRN_W,
                                         height=self.BUTT_SCRN_H,
                                         text=place_to_insert,
                                         padx=self.BUTT_SCRN_PX,
                                         pady=self.BUTT_SCRN_PY,
                                         command=select_screen_call))

        self.screenButtons[place_to_insert].grid(row=1, column=place_to_insert)

        # Add the delay box to the screen array and put it in the correct
        # display position
        self.delayIntVars.insert(place_to_insert,
                                 Tkinter.IntVar(
                                     value=delay))

        self.delayTextBoxes.insert(
            place_to_insert,
            Entry(self.canvas_frame,
                  width=self.ENTRY_DLY_W,
                  textvariable=self.delayIntVars[place_to_insert]
                  )
        )

        self.delayTextBoxes[place_to_insert].grid(
            row=2,
            column=place_to_insert,
            sticky=N+S)

        # Everything gets renumbered since things go weird when adding after
        # removing from the middle
        self.reassign_button_callbacks(0, self.layouts.length())

        # Update the canvas to add the new items then recalculate the
        # scroll region to include them
        self.canvas_frame.update_idletasks()
        self.scroll_canvas.config(scrollregion=self.scroll_canvas.bbox(
            Tkinter.ALL))

        # When loading a saved file always focusing on new slows it down and
        # has been known to lock up the program
        if focus_on_new:
            self.select_screen(place_to_insert)

        assert isinstance(self.removeButton, Button)
        self.removeButton.config(state=Tkinter.NORMAL)

    def remove_screen(self, screen_number=-1, update_display=True):
        """Removes a single screen.

        This function also updates all of the other screen button numbers
        to reflect this change and then selects and displays the next screen.

        Args:
            screen_number: The number of the screen to remove. If the value
                is negative, the screen removed is the current screen,
            update_display: Should the button grid be refreshed?
        """

        if screen_number < 0:
            screen_number = self.selectedScreen
        elif not self.layouts.validate_index(screen_number):
            return

        assert isinstance(self.main_menu_bar, buttGrid_GUI.MenuBar)
        self.main_menu_bar.noChangesAfterSave = False
        if self.layouts.length() != 1:
            # Remove the screen from the layout list
            self.layouts.remove_screen(screen_number)

            # Remove the delay text boxes and screen select buttons
            self.screenButtons[screen_number].grid_forget()
            del self.screenButtons[screen_number]

            self.delayTextBoxes[screen_number].grid_forget()
            del self.delayTextBoxes[screen_number]
            del self.delayIntVars[screen_number]

            # If the currently selected screen was removed, then select the
            # previous screen
            if (self.selectedScreen == screen_number) and update_display:
                if self.selectedScreen > 0:
                    self.selectedScreen -= 1
                else:
                    self.selectedScreen = 0

                self.select_screen(self.selectedScreen, True)

        if self.layouts.length() == 1:
            assert isinstance(self.removeButton, Button)
            self.removeButton.config(state=Tkinter.DISABLED)

        # Update the canvas to remove the just removed item then
        # reduce the scrollable region
        self.canvas_frame.update_idletasks()
        self.scroll_canvas.config(scrollregion=self.scroll_canvas.bbox(
            Tkinter.ALL))

        # Reassign all callback functions at the point of removal to the end
        self.reassign_button_callbacks(screen_number, self.layouts.length())

    def duplicate_screen(self, source=-1, destination=-1):
        """Duplicates the given screen.

        The duplicated source screen is placed in the destination location

        Args:
            source: The screen that will be duplicated. If value is negative,
                the currently visible screen will be the source
            destination: The position where the duplicated screen will
                be placed. If no value is given or value given is negative,
                duplicated screen will be put after the source screen
        """
        if source < 0:
            source = self.selectedScreen

        if destination < 0:
            destination = source + 1

        self.store_current_screen()

        self.insert_new_screen(destination,
                               self.layouts.get_pattern(source),
                               self.layouts.get_delay(source))

        self.delayIntVars[destination].set(self.layouts.get_delay(destination))
        self.theButtGrid.set_button_states(self.layouts.get_pattern(source))

    def remove_all_screens(self):
        """Removes all screens and gives the user a blank canvas to place
        their masterpiece on"""
        # @TODO: Make more efficient. Looping the remove repaints each remove
        for screen in range(self.layouts.length() - 1):
            self.remove_screen(0, update_display=False)
        self.theButtGrid.clear_grid()
        self.layouts.set_screen(self.layouts.BLANK_DISPLAY,
                                self.settings.default_delay,
                                0)

        self.selectedScreen = 0

        self.delayIntVars[0].set(self.layouts.get_delay(0))

    def store_current_screen(self):
        """ Stores the visible pattern and delay in the layout object

        :return: True, if successful , False otherwise
        """
        curr_scrn_state = self.theButtGrid.get_button_states_binary()

        return self.layouts.set_screen(
            curr_scrn_state[0:ButtonGrid.NBR_OF_ROWS],
            self.delayIntVars[self.selectedScreen].get(),
            self.selectedScreen)

    def reassign_button_callbacks(self, start, end):
        """ Reassigns the callback from the given start point to the end

        :param start: Point to start at
        :param end: First point to no redo
        :return: None
        """

        for i in range(start, end):
            self.screenButtons[i].config(text=i)
            select_screen_call = self._layout_button_handler(i)
            self.screenButtons[i].config(command=select_screen_call)
            self.screenButtons[i].grid(column=i)

            # When focus is put on entry, select that screen
            def callback(event, index=i):
                self.select_screen(index, False)
                pass

            # Never a need to tab onto a screen select button, so always
            # go to its delay
            self.screenButtons[i].bind("<FocusIn>", callback)
            self.delayTextBoxes[i].bind("<FocusIn>", callback)

            self.delayTextBoxes[i].grid(column=i)

            # Change the tab order so screen 0 button is first tab, then
            # screen 0 delay, then screen 1 button, etc.
            self.screenButtons[i].lift()
            self.delayTextBoxes[i].lift()

    def define_remove_button(self, button_for_removing):
        """Links the remove button to this class"""
        assert isinstance(button_for_removing, Button)
        self.removeButton = button_for_removing

    def set_delay_box_state(self, state_to_set):
        """Sets all of the delay text boxes to the given state

        Args:
            state_to_set: The state to set the text boxes to. Options
                Tkinter.DISABLED
                Tkinter.NORMAL
        """
        for screenNumber in range(len(self.delayTextBoxes) - 1):
            self.delayTextBoxes[screenNumber]["state"] = state_to_set

    def shift_pattern(self, direction, rotate=False):
        """
        Shifts the pattern in the given direction one cell

        Horizontal shifts are reversed as the left most column on the board
        is actually the LSb or right most bit in the layout

        :param direction: Direction to rotate
        :param rotate: True if the row or column that disappeared should
                        reappear on opposite side
        :return: None. If invalid inputs are given nothing will happen.
                    No error.
        """

        if direction == self.SHIFT_UP:
            direction = buttGrid_Layout.BoardLayout.SHIFT_UP
        elif direction == self.SHIFT_RIGHT:
            direction = buttGrid_Layout.BoardLayout.SHIFT_LEFT
        elif direction == self.SHIFT_DOWN:
            direction = buttGrid_Layout.BoardLayout.SHIFT_DOWN
        elif direction == self.SHIFT_LEFT:
            direction = buttGrid_Layout.BoardLayout.SHIFT_RIGHT
        else:
            return

        if not isinstance(rotate, bool):
            return

        self.store_current_screen()
        self.layouts.shift_screen(self.selectedScreen,
                                  direction,
                                  rotate=rotate,
                                  amount=1)
        self.theButtGrid.set_button_states(self.layouts.get_pattern(
            self.selectedScreen))

        self.main_menu_bar.noChangesAfterSave = False


class ButtonGrid(Tkinter.Frame):
    """This class handles the grid of buttons that represent the LEDs.

    Attributes:
        buttons: Array of buttons used to represent a single pixel of a
            display.
        NBR_OF_COLUMNS: Number of columns in the pattern
        NBR_OF_ROWS: Number of rows in the pattern
    """
    buttons = []
    NBR_OF_COLUMNS = 10
    NBR_OF_ROWS = 10

    def __init__(self, master=None):
        """Initializes the button frame, button array, and draws the grid
        of buttons.
        """

        Tkinter.Frame.__init__(self, master)

        # Really descendants of Frame() but need to be defined here before
        # using them elsewhere in object
        self.theMenuBar = None  # Really buttGrid_GUI.MenuBar
        self.layoutScroller = None  # Really a LayoutScroller

        # Create and draw the button grid
        for r in range(self.NBR_OF_ROWS):
            a_button_row = []
            for c in range(self.NBR_OF_COLUMNS):
                button_toggle_call = self.change_box_state(r, c)
                square = Button(self,
                                text='%s,%s' % (r, c),
                                width=1,
                                height=2,
                                padx=0,
                                pady=0,
                                borderwidth=1,
                                background='black',
                                command=button_toggle_call)
                # self.buttons[r][c] = square
                a_button_row.append(square)
                square.grid(row=r,
                            column=c,
                            sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)
            self.buttons.append(a_button_row)

            # Allow the rows to stretch with the frame
            Tkinter.Grid.rowconfigure(self, r, weight=1)

        # Allow the columns to stretch with the frame
        for c in range(self.NBR_OF_COLUMNS):
            Tkinter.Grid.columnconfigure(self, c, weight=1)

    def set_menu_bar(self, in_menu_bar):
        """Sets the local reference to the menu bar"""
        assert isinstance(in_menu_bar, buttGrid_GUI.MenuBar)
        self.theMenuBar = in_menu_bar

    def set_layout_scroller(self, in_layout_scroller):
        """Sets the local reference to layout scroller"""

        assert isinstance(in_layout_scroller, LayoutScroller)
        self.layoutScroller = in_layout_scroller

    def change_box_state(self, row, column):
        """Handle click events for the LED buttons.

        It changes the button's color.

        Args:
            row: Row of the cell to toggle.
            column: Column of the cell to toggle.
        """

        def change_it():
            assert isinstance(self.theMenuBar, buttGrid_GUI.MenuBar)
            self.theMenuBar.noChangesAfterSave = False
            # square = self.buttons[(row, column)]
            square = self.buttons[row][column]
            if square["background"] == 'black':
                square["background"] = 'white'
            else:
                square["background"] = 'black'

        return change_it

    def get_button_states_array(self):
        """Creates a 10x10 array indicating the state of each of the LED
        buttons.

        If a cell is white, the value appended is one (1), otherwise it
        is zero (0).

        Returns:
            A 10x10 array indicating the states of all of the pixels in the
                display array.
        """
        button_state_list = []
        for r in range(self.NBR_OF_ROWS):
            current_row = []
            for c in range(0, self.NBR_OF_COLUMNS):
                # Convert the states to bools
                if self.buttons[r][c]["background"] == 'white':
                    current_row.append(1)
                else:
                    current_row.append(0)
            button_state_list.append(current_row)

        return button_state_list

    def get_button_states_binary(self):
        """Returns an 1x10 array of values for each row in the current
        display.

        Each item in the array represents one row of the array. Each row
        is a number. The output values follow the same pattern as those
        returned by get_button_states_array.

        Returns:
            A 1x10 array indicating the states of all of the pixels in the
                display area.
        """
        # @TODO: Return only the patten
        button_states = self.get_button_states_array()
        array_of_row_values = []
        for row in range(self.NBR_OF_ROWS):
            row_values = 0
            for column in range(self.NBR_OF_COLUMNS):
                row_values += (button_states[row][column] << column)
            array_of_row_values.append(row_values)

        # Append the delay to the button state array
        assert isinstance(self.layoutScroller, LayoutScroller)
        # array_of_row_values.append(
        #     self.layoutScroller.screenArray[
        #         self.layoutScroller.selectedScreen][10])
        array_of_row_values.append(
            self.layoutScroller.layouts.get_delay(
                self.layoutScroller.selectedScreen)
        )
        return array_of_row_values

    def set_button_states(self, list_of_states):
        """ Sets the displayed grid according to the given array.

        It does the opposite of get_button_states_array.

        Args:
            list_of_states: A 1x10 list of numbers representing the pattern to
                be displayed. Each row is a 10 bit value. Each bit
                indicates the on/off state of its corresponding button.
        """
        for row in range(self.NBR_OF_ROWS):
            for column in range(self.NBR_OF_COLUMNS):
                if ((list_of_states[row] >> column) & 0x1) == 1:
                    self.buttons[row][column]["background"] = 'white'
                else:
                    self.buttons[row][column]["background"] = 'black'

    def clear_grid(self):
        """Sets all of the LED buttons in to their off states"""
        for row in range(self.NBR_OF_ROWS):
            for column in range(self.NBR_OF_COLUMNS):
                self.buttons[row][column]["background"] = 'black'

        assert isinstance(self.theMenuBar, buttGrid_GUI.MenuBar)
        self.theMenuBar.noChangesAfterSave = False

    def invert_button_states(self):
        """Inverts the state of all of the currently displayed buttons"""
        # Store the current pattern in the layout list
        self.layoutScroller.store_current_screen()

        # Invert the pattern
        self.layoutScroller.layouts.invert_screen(
            self.layoutScroller.selectedScreen)

        # Update the button states to reflect the inverted pattern
        self.set_button_states(
            self.layoutScroller.layouts.get_pattern(
                self.layoutScroller.selectedScreen))

        assert isinstance(self.theMenuBar, buttGrid_GUI.MenuBar)
        self.theMenuBar.noChangesAfterSave = False

    def toggle_row_state(self, row):
        """ Toggles the state of the given row's buttons for the current
        screen
        :param row: Row to toggle
        :return: None
        """

        # Store the current pattern in the layout list
        self.layoutScroller.store_current_screen()

        # Determine the current state
        all_equal, all_on = self.layoutScroller.layouts.get_row_state(
            self.layoutScroller.selectedScreen,
            row
        )

        # Turn them off only if all are already on, otherwise turn all on
        self.layoutScroller.layouts.set_row_state(
            self.layoutScroller.selectedScreen,
            row,
            not (all_equal & all_on))

        # Update the button states to reflect the new pattern
        self.set_button_states(
            self.layoutScroller.layouts.get_pattern(
                self.layoutScroller.selectedScreen))

        assert isinstance(self.theMenuBar, buttGrid_GUI.MenuBar)
        self.theMenuBar.noChangesAfterSave = False

    def toggle_column_state(self, column):
        """ Toggles the state of the given column's buttons for the current
        screen
        :param column: Column to toggle
        :return: None
        """

        # Store the current pattern in the layout list
        self.layoutScroller.store_current_screen()

        # Determine the current state
        all_equal, all_on = self.layoutScroller.layouts.get_column_state(
            self.layoutScroller.selectedScreen,
            column
        )

        # Turn them off only if all are already on, otherwise turn all on
        self.layoutScroller.layouts.set_column_state(
            self.layoutScroller.selectedScreen,
            column,
            not (all_equal & all_on))

        # Update the button states to reflect the new pattern
        self.set_button_states(
            self.layoutScroller.layouts.get_pattern(
                self.layoutScroller.selectedScreen))

        assert isinstance(self.theMenuBar, buttGrid_GUI.MenuBar)
        self.theMenuBar.noChangesAfterSave = False

    def set_button_state(self, in_state):
        """Sets all of the button grids to the given state

        When the buttons are disabled the text is removed since it would be
        visible otherwise.

        Args:
            in_state: The state to set the buttons to. Options
                Tkinter.DISABLED
                Tkinter.NORMAL
        """
        for row in range(self.NBR_OF_ROWS):
            for column in range(self.NBR_OF_COLUMNS):
                self.buttons[row][column]["state"] = in_state
                if in_state == Tkinter.DISABLED:
                    self.buttons[row][column]["text"] = ''
                else:
                    self.buttons[row][column]["text"] = '%s,%s' % (row, column)
