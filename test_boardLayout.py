from unittest import TestCase
from buttGrid_Layout import BoardLayout

__author__ = 'Meyer'


class TestBoardLayout(TestCase):

    def test_insert_screen(self):
        board_layout = BoardLayout()

        # Test valid inserts
        # print("Test BoardLayout.insert_screen() with valid parameters")
        test_list = []
        for i in range(0, 10):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.insert_screen(test_pattern, i + 1, i))
            test_list.append(test_pattern)

        # Verify values were set as expected
        # print("Test BoardLayout.insert_screen() verifying correct data was "
        #       "inserted")
        for i in range(0, 10):
            self.assertEqual(board_layout.get_screen(i),
                             (test_list[i], i + 1))

        # Verify negative indices
        test_pattern = [(x + 1) * 2 for x in test_pattern]
        self.assertTrue(board_layout.insert_screen(test_pattern, 99, -1))
        self.assertEqual(board_layout.get_screen(-2),
                         (test_pattern, 99))

        # Verify fail with invalid index using a known valid pattern and delay
        # print("Test BoardLayout.insert_screen() with invalid indices")
        # self.assertFalse(board_layout.insert_screen(test_list[0], 1, -2))
        self.assertFalse(board_layout.insert_screen(test_list[0], 1, 100))

        # Verify fail with invalid pattern, valid delay, and valid index
        # print("Test BoardLayout.insert_screen() with an invalid pattern")
        test_index = 1
        self.assertFalse(board_layout.insert_screen(10, 1, test_index))
        # Verify values at index did not change
        self.assertEqual(board_layout.get_screen(test_index),
                         (test_list[test_index], test_index + 1))

        # Verify fail with valid pattern, invalid delay, and valid index
        # print("Test BoardLayout.insert_screen() with an invalid delay")
        self.assertFalse(board_layout.insert_screen(test_list[test_index],
                                                    0,
                                                    test_index))
        # Verify values at index did not change
        self.assertEqual(board_layout.get_screen(test_index),
                         (test_list[test_index], test_index + 1))

    def test_set_screen(self):
        board_layout = BoardLayout()

        # Insert valid data
        test_list = []
        count = 10
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # Test valid data
        # print("Test BoardLayout.set_screen() with a valid pattern and delay")
        seed = 11
        index = 8
        test_pattern = range(seed * count, (seed + 1) * count)
        self.assertTrue(board_layout.set_screen(test_pattern, seed, index))

        # Verify set occurred
        self.assertEqual(board_layout.get_screen(index), (test_pattern, seed))

        # print("Test BoardLayout.set_screen() with a negative index")
        self.assertNotEqual(board_layout.get_screen(count - 1),
                            (test_pattern, seed))
        self.assertTrue(board_layout.set_screen(test_pattern, seed, -1))
        self.assertEqual(board_layout.get_screen(count - 1),
                         (test_pattern, seed))

        # Test with index out of range
        # print("Test BoardLayout.set_screen() with invalid indices")
        self.assertFalse(board_layout.set_screen(test_pattern, seed, 100))

        # Test with invalid pattern
        index = 5

        # Int instead of list
        # print("Test BoardLayout.set_screen() with an int in lieu of a pattern")
        self.assertFalse(board_layout.set_screen(1, 1, index))
        self.assertEqual(board_layout.get_screen(index),
                         (test_list[index], index + 1))

        # Wrong length list
        # print("Test BoardLayout.set_screen() with the wrong number of columns")
        test_pattern = range(i * 10, (i + 1) * 9)
        self.assertFalse(board_layout.set_screen(test_pattern, 1, index))
        self.assertEqual(board_layout.get_screen(index),
                         (test_list[index], index + 1))

        # Wrong length rows in correct length list
        # print("Test BoardLayout.set_screen() wrong length rows")
        test_pattern = range(i * 1000, (i + 1) * 1000)
        self.assertFalse(board_layout.set_screen(test_pattern, 1, index))
        self.assertEqual(board_layout.get_screen(index),
                         (test_list[index], index + 1))

        # Test with invalid delay
        # print("Test BoardLayout.set_screen() with invalid delays")
        self.assertFalse(board_layout.set_screen(test_list[index], 0, index))
        self.assertEqual(board_layout.get_screen(index),
                         (test_list[index], index + 1))

    def test_get_screen(self):
        board_layout = BoardLayout()

        # Insert valid data
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # Verify valid indices
        # print("Test BoardLayout.get_screen() with valid indices")
        for i in range(0, count):
            self.assertEqual(board_layout.get_screen(i),
                             (test_list[i], i + 1))

        # Test with a negative index
        self.assertEqual(board_layout.get_screen(-1),
                         (test_list[-1], 10))

        # Verify invalid indices fail
        # print("Test BoardLayout.get_screen() with invalid indices")
        self.assertFalse(board_layout.get_screen(count))

    def test_get_pattern(self):
        board_layout = BoardLayout()

        # Insert valid data
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # Verify valid indices
        # print("Test BoardLayout.get_pattern() with valid indices")
        for i in range(0, count):
            self.assertEqual(board_layout.get_pattern(i),
                             test_list[i])

        # Test with a negative index
        self.assertEqual(board_layout.get_pattern(-1),
                         test_list[-1])

        # Verify invalid indices fail
        # print("Test BoardLayout.get_pattern() with invalid indices")
        self.assertFalse(board_layout.get_pattern(count))

    def test_get_delay(self):
        board_layout = BoardLayout()

        # Insert valid data
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # Verify valid indices
        # print("Test BoardLayout.get_delay() with valid indices")
        for i in range(0, count):
            self.assertEqual(board_layout.get_delay(i),
                             i + 1)

        # Test with a negative index
        self.assertEqual(board_layout.get_delay(-1),
                         i + 1)

        # Verify invalid indices fail
        # print("Test BoardLayout.get_delay() with invalid indices")
        self.assertFalse(board_layout.get_delay(count))


    def test_remove_screen(self):
        board_layout = BoardLayout()

        # Insert valid data
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # print("Test BoardLayout.remove_screen() with a valid index")
        removed_index = 1
        self.assertTrue(board_layout.remove_screen(removed_index))
        # Verify the items shifts down one
        self.assertEqual(board_layout.get_screen(removed_index),
                         (test_list[removed_index + 1], removed_index + 2))
        self.assertEqual(board_layout.get_screen(removed_index + 1),
                         (test_list[removed_index + 2], removed_index + 3))

        # Test with a negative index
        self.assertTrue(board_layout.remove_screen(-1))

        # print("Test BoardLayout.remove_screen() with invalid indices")
        self.assertFalse(board_layout.remove_screen(count))

    def test_duplicate_screen(self):
        board_layout = BoardLayout()

        # Insert valid data
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)

        # print("Test BoardLayout.duplicate_screen() with two valid indices")
        self.assertTrue(board_layout.duplicate_screen(0, 1))
        self.assertEqual(board_layout.get_screen(1),
                         board_layout.get_screen(0))

        # print("Test BoardLayout.duplicate_screen() with a negative "
        #       "source index")
        self.assertTrue(board_layout.duplicate_screen(-1, 4))
        self.assertEqual(board_layout.get_screen(-1),
                         board_layout.get_screen(4))

        # print("Test BoardLayout.duplicate_screen() with a negative "
        #       "destination index")
        self.assertTrue(board_layout.duplicate_screen(2, -1))
        self.assertEqual(board_layout.get_screen(2),
                         board_layout.get_screen(-2))

        # print("Test BoardLayout.duplicate_screen() with an invalid "
        #       "source index")
        start_length = board_layout.length()
        valid_index = 1
        start_value = board_layout.get_screen(valid_index)
        self.assertFalse(board_layout.duplicate_screen(100, valid_index))
        self.assertEqual(board_layout.length(), start_length)
        self.assertEqual(board_layout.get_screen(valid_index),
                         start_value)

        # print("Test BoardLayout.duplicate_screen() with an invalid "
        #       "destination index")
        start_length = board_layout.length()
        self.assertFalse(board_layout.duplicate_screen(1, 100))
        self.assertEqual(board_layout.length(), start_length)

    def test_length(self):
        board_layout = BoardLayout()

        # Insert valid data
        # print("Test BoardLayout.length() when adding patterns")
        count = 10
        test_list = []
        for i in range(0, count):
            test_pattern = range(i * 10, (i + 1) * 10)
            self.assertTrue(board_layout.append_screen(test_pattern, i + 1))
            test_list.append(test_pattern)
            self.assertEqual(board_layout.length(), i + 1)

        # print("Test BoardLayout.length() when removing patterns")
        for i in range(0, count):
            self.assertTrue(board_layout.remove_screen(0))
            self.assertEqual(board_layout.length(), 9 - i)

    def test_shift_screen_up(self):
        board_layout = BoardLayout()

        test_pattern = range(10, 20)
        delay = 10
        index = 0

        board_layout.append_screen(test_pattern, delay)

        # Test up rotate with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, True, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test up shift (no-rotate) with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, False, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test up rotate with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, True, 1)

        expected = range(11, 20) + [10]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up shift (no-rotate) with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, False, 1)

        expected = range(11, 20) + [0]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up rotate with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, True, 105)

        expected = range(15, 20) + range(10, 15)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up shift (no-rotate) with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, False, 105)

        expected = [0] * board_layout.COL_SIZE
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up rotate with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, True, -1)

        expected = [19] + range(10, 19)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up shift (no-rotate) with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, False, -1)

        expected = [0] + range(10, 19)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up rotate with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, True, -105)

        expected = range(15, 20) + range(10, 15)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test up shift (no-rotate) with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_UP, False, -105)

        expected = [0] * board_layout.COL_SIZE
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

    def test_shift_screen_down(self):
        board_layout = BoardLayout()

        test_pattern = range(10, 20)
        delay = 10
        index = 0

        board_layout.append_screen(test_pattern, delay)

        # Test down rotate with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, True, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test down shift (no-rotate) with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, False, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test down rotate with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, True, 1)

        expected = [19] + range(10, 19)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down shift (no-rotate) with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, False, 1)

        expected = [0] + range(10, 19)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down rotate with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, True, 105)

        expected = range(15, 20) + range(10, 15)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down shift (no-rotate) with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, False, 105)

        expected = [0] * board_layout.COL_SIZE
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down rotate with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, True, -1)

        expected = range(11, 20) + [10]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down shift (no-rotate) with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, False, -1)

        expected = range(11, 20) + [0]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down rotate with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, True, -105)

        expected = range(15, 20) + range(10, 15)
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test down shift (no-rotate) with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_DOWN, False, -105)

        expected = [0] * board_layout.COL_SIZE
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

    def test_shift_screen_left(self):
        board_layout = BoardLayout()

        test_pattern = [2 ** x for x in range(0, 10)]
        delay = 10
        index = 0

        board_layout.append_screen(test_pattern, delay)

        # Test left rotate with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, True, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test left shift (no-rotate) with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, False, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test left rotate with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, True, 1)

        expected = [2 ** x for x in range(1, 10)] + [1]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left (non-rotate) shift with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, False, 1)

        expected = [2 ** x for x in range(1, 10)] + [0]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left rotate with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, True, 105)

        expected = [2 ** x for x in range(5, 10)] + \
                   [2 ** x for x in range(0, 5)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left (non-rotate) shift with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, False, 105)

        expected = [0] * 10
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left rotate with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, True, -1)

        expected = [2 ** 9] + [2 ** x for x in range(0, 9)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left (non-rotate) shift with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, False, -1)

        expected = [0] + [2 ** x for x in range(0, 9)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left rotate with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, True, -105)

        expected = [2 ** x for x in range(5, 10)] + \
                   [2 ** x for x in range(0, 5)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test left (non-rotate) shift with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_LEFT, False, -105)

        expected = [0] * 10
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

    def test_shift_screen_right(self):
        board_layout = BoardLayout()

        test_pattern = [2 ** x for x in range(0, 10)]
        delay = 10
        index = 0

        board_layout.append_screen(test_pattern, delay)

        # Test right rotate with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, True, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test right shift (no-rotate) with a zero shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, False, 0)

        expected = test_pattern
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Test right rotate with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, True, 1)

        expected = [2 ** 9] + [2 ** x for x in range(0, 9)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right (non-rotate) shift with small positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, False, 1)

        expected = [0] + [2 ** x for x in range(0, 9)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right rotate with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, True, 105)

        expected = [2 ** x for x in range(5, 10)] + \
                   [2 ** x for x in range(0, 5)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right (non-rotate) shift with large positive shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, False, 105)

        expected = [0] * 10
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right rotate with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, True, -1)

        expected = [2 ** x for x in range(1, 10)] + [1]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right (non-rotate) shift with small negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, False, -1)

        expected = [2 ** x for x in range(1, 10)] + [0]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right rotate with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, True, -105)

        expected = [2 ** x for x in range(5, 10)] + \
                   [2 ** x for x in range(0, 5)]
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

        # Test right (non-rotate) shift with large negative shift amount
        board_layout.shift_screen(index, BoardLayout.SHIFT_RIGHT, False, -105)

        expected = [0] * 10
        self.assertEqual(board_layout.get_pattern(index), expected)

        # Reset screen
        self.assertTrue(board_layout.set_screen(test_pattern,
                                                delay,
                                                index))

    def test_set_row_state(self):
        """ Tests the set_row_state_function

        :return: None
        """
        board_layout = BoardLayout()

        board_layout.append_screen([0] * 10, 20)

        # Test a valid page and index set
        expected_pattern = [0] * 10
        for i in range(0, 10):
            self.assertTrue(board_layout.set_row_state(0, i, True))

            expected_pattern[i] = 0x3FF
            self.assertEqual(board_layout.get_pattern(0), expected_pattern)

        # Valid clear
        for i in range(0, 10):
            self.assertTrue(board_layout.set_row_state(0, i, False))

            expected_pattern[i] = 0x00
            self.assertEqual(board_layout.get_pattern(0), expected_pattern)

        # Invalid arguments
        self.assertFalse(board_layout.set_row_state(100, 0, True))
        self.assertFalse(board_layout.set_row_state(0, 100, True))
        self.assertFalse(board_layout.set_row_state(0, 0, 1))

    def test_set_column_state(self):
        """ Tests the set_column_state_function

        :return: None
        """
        board_layout = BoardLayout()

        board_layout.append_screen([0] * 10, 20)

        # Test a valid page and index set
        expected_pattern = [0] * 10
        for i in range(0, 10):
            self.assertTrue(board_layout.set_column_state(0, i, True))

            expected_pattern = [2**i] * 10
            self.assertEqual(board_layout.get_pattern(0), expected_pattern)

            self.assertTrue(board_layout.set_column_state(0, i, False))

            expected_pattern = [0x00] * 10
            self.assertEqual(board_layout.get_pattern(0), expected_pattern)

        # Invalid arguments
        self.assertFalse(board_layout.set_row_state(100, 0, True))
        self.assertFalse(board_layout.set_row_state(0, 100, True))
        self.assertFalse(board_layout.set_row_state(0, 0, 1))
