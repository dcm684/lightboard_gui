__author__ = 'Meyer'


class BoardLayout(object):
    """Handles the loading and storage of all of the LED patterns

    Attributes:
        _layouts: List of layouts
        _delays: List of delays for each layout

        ROW_SIZE: Number of LEDs in a row
        COL_SIZE: Number of LEDs in a column
        SHIFT_UP: Value used with shift to shift rows up
        SHIFT_RIGHT: Value used with shift to shift columns right
        SHIFT_DOWN: Value used with shift to shift rows down
        SHIFT_LEFT: Value used with shift to shift columns left
        BLANK_DISPLAY: An all of pattern
    """

    ROW_SIZE = 10
    COL_SIZE = 10

    SHIFT_UP = "up"
    SHIFT_RIGHT = "right"
    SHIFT_DOWN = "down"
    SHIFT_LEFT = "left"

    BLANK_DISPLAY = [0] * ROW_SIZE

    def __init__(self):
        self._layouts = []
        self._delays = []

    def validate_index(self, index):
        """Returns true if an index exists in the list of screens"""
        return isinstance(index, int) and index < len(self._layouts)

    def get_screen(self, index):
        """Returns the requested screen

        :param index: Index of screen to load
        :return: Requested pattern and delay if index was
                    valid, False otherwise. Valid results will be in a tuple
                    of the form (pattern, delay)
        """
        if self.validate_index(index):
            return self._layouts[index], self._delays[index]
        else:
            return False

    def get_pattern(self, index):
        """
        :param index:Returns the pattern for the given index
        :return: The pattern if the index is valid, False otherwise
        """
        if self.validate_index(index):
            return self._layouts[index]
        else:
            return False

    def get_delay(self, index):
        """
        :param index:Returns the delay for the given index
        :return: The delay if the index is valid, False otherwise
        """
        if self.validate_index(index):
            return self._delays[index]
        else:
            return False

    def set_screen(self, pattern, delay, index):
        """Sets the screen and delay for the given index

        :param pattern: Pattern to store at the given index. Must be a an
                        array with COL_SIZE positive integers with values
                        between or equal to 0 and 2**ROW_SIZE - 1
        :param delay: Time to hold for the given screen. Must be a positive,
                        non-zero integer
        :param index: Index of the screen to be stored. Negative
                        indexing traverses the lists in reverse.
                        Max is len(_layouts) - 1

        :return: True if set was valid, false otherwise
        """

        success = True

        if self.validate_index(index) and \
                (isinstance(pattern, list)) and \
                (len(pattern) == self.COL_SIZE) and \
                (isinstance(delay, int)) and \
                (delay > 0):

            # Verify
            for aRow in pattern:
                if (not isinstance(aRow, int)) and \
                        (0 <= aRow <= 2**self.ROW_SIZE - 1):
                    success = False
                    break

            # Values were all valid update the layout and delay lists
            if success:
                self._layouts[index] = list(pattern)  # Copy the list
                self._delays[index] = delay

        else:
            success = False

        return success

    def insert_screen(self, pattern, delay, index):
        """Inserts the screen and delay at the given index

        :param pattern: Pattern to store at the given index. Must be a an
                        array with COL_SIZE positive integers with values
                        between or equal to 0 and 2**ROW_SIZE - 1
        :param delay: Time to hold for the given screen. Must be a positive,
                        non-zero integer
        :param index: Index to insert the new screen at. Negative values
                        traverse the list in reverse, just like Python
                        indexing. Item will be added before the item
                        currently at that index. If index is positive new
                        screen will go to that index. If it is negative
                        (e.g. -1), screen will go before it (e.g. -2).
                        Max is len(_layouts) which will cause an
                        append

        :return: True if set was valid, false otherwise
        """

        success = False

        if isinstance(index, int) and index <= len(self._layouts):

            # Use placeholders so we can use set_screen() to validate
            self._layouts.insert(index, [])  # Values will get overwritten
            self._delays.insert(index, 1)  # Value will get overwritten

            # Without decrementing index on negative indices set_screen will
            # set the item after the negative. No need to adjust del index
            if index < 0:
                set_index = index - 1
            else:
                set_index = index

            success = self.set_screen(pattern, delay, set_index)

            # If the set failed, delay the recently added layout and delay
            if not success:
                del self._layouts[index]
                del self._delays[index]

        return success

    def append_screen(self, pattern, delay):
        """Appends the screen and delay to the end of the list

        :param pattern: Pattern to store at the given index. Must be a an
                        array with COL_SIZE positive integers with values
                        between or equal to 0 and 2**ROW_SIZE - 1
        :param delay: Time to hold for the given screen. Must be a positive,
                        non-zero integer
        :return: True if set was valid, false otherwise
        """

        success = self.insert_screen(pattern, delay, self.length())

        return success

    def remove_screen(self, index):
        """ Removes the screen at the given index

        :param index: Index of screen to remove.  Negative values traverse
                        the list in reverse. Max is len(_layouts) - 1
        :return: True if removal was successful
        """

        success = False

        # If the index is valid, delete from the layout and delay lists
        if self.validate_index(index):
            del self._layouts[index]
            del self._delays[index]
            success = True

        return success

    def duplicate_screen(self, from_index, to_index):
        """ Copies a pattern and its delay

        :param from_index: Pattern and delay to duplicate. Negative
                            values traverse the list in reverse. Max is
                            pattern count - 1
        :param to_index: Index to place the duplicate at. Negative
                            values traverse the list in reverse. Max is
                            pattern count - 1.
        :return: True if successful, otherwise false
        """

        success = False

        if self.validate_index(from_index) and self.validate_index(to_index):

            success = self.insert_screen(self._layouts[from_index],
                                         self._delays[from_index],
                                         to_index)

        return success

    def invert_screen(self, index):
        """ Inverts the given screen's pattern

        :param index: Index of screen to invert
        :return: True if successful, otherwise false
        """

        if self.validate_index(index):
            xor_value = 2**self.COL_SIZE - 1

            # Invert each row by XORing the value
            for row in range(self.ROW_SIZE):
                self._layouts[index][row] ^= xor_value

            return True
        else:
            return False

    def shift_screen(self, index, direction, rotate=False, amount=1):
        """ Shifts the screen in the given direction by the given amount

        :param index: Screen to shift
        :param direction: Direction to shift the screen
        :param rotate: Should the cells shifted out be brought back in to fill
                        the empty cells. If False, empty cells will be false
        :param amount: Number of cells to shift in given direction
        :return: True, if screen index was valid otherwise False is returned
        """

        # Verify that the screen index was valid
        if not self.validate_index(index):
            return False

        # Down shifts are just negative up shifts
        if direction == self.SHIFT_DOWN:
            direction = self.SHIFT_UP
            amount *= -1

        elif direction == self.SHIFT_RIGHT:
            direction = self.SHIFT_LEFT
            amount *= -1

        if direction == self.SHIFT_UP:

            # When shifting in a negative direction, you need to mod against
            # a negative value otherwise the result will be wrong
            if rotate:
                if amount < 0:
                    amount %= -self.COL_SIZE
                else:
                    amount %= self.COL_SIZE

            # Non-rotate shift is greater than COL_SIZE or less than -COL_SIZE
            # everything will be shifted out leaving only zeroes
            if -self.COL_SIZE < amount < self.COL_SIZE:
                if amount < 0:
                    temp = self._layouts[index][:amount]
                else:
                    temp = self._layouts[index][amount:]

                # Fill in the empty rows
                if rotate:
                    if amount < 0:
                        carry_over = self._layouts[index][amount:]
                    else:
                        carry_over = self._layouts[index][:amount]
                else:
                    carry_over = ([0] * (self.COL_SIZE - len(temp)))

                # Attach the carryover
                if amount < 0:
                    # Negative shifts put the carry over at the front
                    temp = carry_over + temp
                else:
                    # Positive shifts put the carry over at the end
                    temp += carry_over

            else:
                temp = [0] * self.COL_SIZE

            self._layouts[index] = temp

        elif direction == self.SHIFT_LEFT:
            # When shifting in a negative direction, you need to mod against
            # a negative value otherwise the result will be wrong
            if rotate:
                if amount < 0:
                    amount %= -self.ROW_SIZE
                else:
                    amount %= self.ROW_SIZE

            for row in range(0, len(self._layouts[index])):

                # Create a working variable so original can still be accessed
                temp = self._layouts[index][row]

                # print bin(temp),

                # Do the actual shift, shifts may result in too large of a
                # number
                if amount < 0:
                    # Copy the a shifted row's value to the MSb position to
                    # simplify carry over when rotating
                    if rotate:
                        temp |= (temp << self.ROW_SIZE)

                    temp >>= (amount * -1)
                else:
                    temp <<= amount
                # print bin(temp),

                # OR the overflow with the LSB when a rotate was desired
                if rotate:
                    temp |= (temp >> self.ROW_SIZE)
                # print bin(temp),

                # AND out the values outside of the row-size range
                temp &= (2**self.ROW_SIZE - 1)
                # print bin(temp)

                # Replace the value
                self._layouts[index][row] = temp

            else:
                return False

        return True

    def get_row_state(self, screen, row):
        """ Sees if all of the buttons in the given row for the given
            screen are the same
        :param screen: Screen that contains the row whose state is desired
        :param row: Row whose state is desired
        :return: A tuple
                    True if all buttons in row are the same, otherwise false
                    True if all buttons in row are on
        """
        if self.validate_index(screen) \
                and 0 <= row < self.COL_SIZE:

            all_equal = False
            all_on = False
            if self._layouts[screen][row] == 0:
                all_equal = True
            elif self._layouts[screen][row] == 2**self.ROW_SIZE-1:
                all_equal = True
                all_on = True

            return all_equal, all_on

    def set_row_state(self, screen, row, state):
        """ Sets the given row to the given state

        :param screen: Index of screen to modify
        :param row: Row number to modify
        :param state: State as a bool to set the row to
        :return: True if row was set, otherwise false
        """
        if self.validate_index(screen) \
                and 0 <= row < self.COL_SIZE \
                and isinstance(state, bool):
            if state:
                value = 2**self.ROW_SIZE - 1
            else:
                value = 0

            self._layouts[screen][row] = value

            return True
        else:
            return False

    def get_column_state(self, screen, column):
        """ Sees if all of the buttons in the given column for the given
            screen are the same
        :param screen: Screen that contains the column whose state is desired
        :param column: Column whose state is desired
        :return: A tuple
                    True if all buttons in column are the same, otherwise false
                    True if all buttons in column are on
        """
        if self.validate_index(screen) \
                and 0 <= column < self.COL_SIZE:

            all_equal = True

            first_row_value = self._layouts[screen][0] & 2**column

            if first_row_value == 0:
                all_on = False
            else:
                all_on = True

            for row in range(1, self.COL_SIZE):
                # Current row is 0 for column
                if self._layouts[screen][row] & 2**column == 0:
                    # But first row value was not zero
                    if first_row_value != 0:
                        # Not all are matching so you can quit here
                        all_equal = False
                        all_on = False
                # Current row is not 0
                else:
                    # But first row was, not all are matching
                    if first_row_value == 0:
                        all_equal = False
            return all_equal, all_on

    def set_column_state(self, screen, column, state):
        """ Sets the given column to the given state

        :param screen: Index of screen to modify
        :param column: Column number to modify
        :param state: State as a bool to set the column to
        :return: True if row was set, otherwise false
        """
        if self.validate_index(screen) \
                and 0 <= column < self.ROW_SIZE \
                and isinstance(state, bool):

            for row in range(self.COL_SIZE):
                if state:
                    # OR to set
                    self._layouts[screen][row] |= 2**column
                else:
                    # AND to clear
                    self._layouts[screen][row] &= ~(2**column)

            return True
        else:
            return False

    def length(self):
        """ Returns the number of LED patterns
        :return: Number of LED patterns
        """
        return len(self._layouts)

    def __str__(self):
        out_str = "Length " + str(self.length()) + "\r\n"
        for i in range(0, self.length()):
            out_str += "%3i " % i
            out_str += str(self._layouts[i]) + " "
            out_str += str(self._delays[i]) + "\r\n"

        return out_str
