""" Contains the non-button grid portion of the button grid program GUI

The functionality included in this module's classes include creating the
    menu bar and adding the buttons use to manipulate a single display

Copyright: Delta Charlie Mike 2010
License: For use by the author, Christopher Meyer, only
Author: Christopher Meyer
"""

import Tkinter
import sys
import tkMessageBox
import pickle
import os

import buttGrid_Grid
import buttGrid_Settings
import buttGrid_Filer
import buttGrid_Layout

from tkFileDialog import asksaveasfilename, askopenfilename

DEFAULT_TITLE = "Light Board Configurator V1.0"
EXPORT_SERIAL_LOC = 2  # Location in the export menu for export to serial
CLEAR_MEM_LOC = 4  # Location in the export menu for clearing EEPROM pattern

PLAY_START_LOC = 1  # Index in the playback menu for play from start
PLAY_HERE_LOC = 2  # Index in the playback menu for play from the
# current position
STOP_PLAY_LOC = 3  # Index in the playback menu for stopping playback


class MenuBar(Tkinter.Frame):
    """Creates the menu bar and handles all of the associated events.

    In addition to drawing the menu bar, this class contains the functions
    used to handle menu bar events. These functions include creating new,
    saving, and opening lightboard programs.

    Attributes:
        defaultExtension: File type of this program.
        fileTypeTuple: File types to be displayed in the file dialogs.
        noChangesAfterSave: Indicates whether the display program has
            been modified since the last time it was saved.
        saveFileName: Name of the file currently being edited
        root: Local Tk instance that the menu is attached to.
        buttonGrid: Local instance of the grid of buttons used to display
            the light grid.
        layout_scroller: Local instance of the class containing all of the
            display screens.
        menu: The top level menu that fileMenu is attached to.
        fileMenu: The file menu.
    """

    defaultExtension = '.lsv'
    fileTypeTuple = [('LightBoard Save Files', defaultExtension)]
    noChangesAfterSave = True

    idPlayback = 0  # id for the after function used to playback the pattern

    def __init__(self, root, in_button_grid, in_layout_scroller,
                 in_pc_interface, use_serial, in_settings, master=None):
        """Initializes the menu bar and its handlers.

        In addition to drawing the menu bar and attaching its handlers, this
        function links class variables to the parent's variables for the
        screen selector and the microcontroller interface.

        Args:
            self: This class.
            root: Tk instance that the menu will be attached to.
            in_button_grid: Instance of a button grid to display and
                what to display it on
            in_layout_scroller: Instance of the class containing all of the
                display screens.
            in_pc_interface: Instance of the class used to communicate with
                the control board.
            use_serial: Boolean indicating whether serial communications
                are to be used
            in_settings: Instance of settings object

        Returns:
            None
        """

        self.saveFileName = ''

        assert isinstance(in_button_grid, buttGrid_Grid.ButtonGrid)
        self.buttonGrid = in_button_grid

        assert isinstance(in_layout_scroller, buttGrid_Grid.LayoutScroller)
        self.layout_scroller = in_layout_scroller

        assert isinstance(in_settings, buttGrid_Settings.SettingsMenu)
        self.settings = in_settings

        self.root = root

        Tkinter.Frame.__init__(self, master)
        self.menu = Tkinter.Menu(root)
        root.config(menu=self.menu)

        # Create and populate the menus
        self.file_menu = Tkinter.Menu(self.menu)
        # Add the file menu
        self.menu.add_cascade(label="File",
                              menu=self.file_menu,
                              underline=0)

        # 0 Add an option for creating a new program
        new_pattern_cmd = self.new_file_handler()
        self.file_menu.add_command(label="New",
                                   command=new_pattern_cmd,
                                   underline=0)
        # 1 Separator
        self.file_menu.add_separator()

        # 2 Add an option for saving using the current file name
        save_cmd = self.save_file_handler(self.saveFileName)
        self.file_menu.add_command(label="Save",
                                   command=save_cmd,
                                   underline=0)

        # 3 Add an option for saving using a new file name
        save_as_cmd = self.save_file_as_handler()
        self.file_menu.add_command(label="Save As...",
                                   command=save_as_cmd,
                                   underline=5)
        # 4 Add an option for opening a file
        open_cmd = self.open_file_handler()
        self.file_menu.add_command(label="Open...",
                                   command=open_cmd,
                                   underline=0)
        # 5 Separator
        self.file_menu.add_separator()

        # 6 Add option for the settings menu
        self.file_menu.add_command(label="Settings",
                                   command=self.settings.show_settings,
                                   underline=6)

        # 7 Separator
        self.file_menu.add_separator()

        # 8 Add an option to exit the program
        self.file_menu.add_command(label="Exit",
                                   command=sys.exit,
                                   underline=1)

        # Add the export menu
        self.exportMenu = Tkinter.Menu(self.menu)
        self.menu.add_cascade(label="Export",
                              menu=self.exportMenu,
                              underline=1)

        # 1 Add an option for exporting the C code for the current program
        def export_code_callback():
            # Save the active screen to the screen array
            self.layout_scroller.store_current_screen()

            in_pc_interface.export_to_code(self.layout_scroller.layouts)

        self.exportMenu.add_command(label="Export Code",
                                    command=export_code_callback,
                                    underline=7)

        # 2 Add an option for communicating with the board to display the
        # current program
        def export_serial_callback():
            # Save the active screen to the screen array
            self.layout_scroller.store_current_screen()

            success, error_string = in_pc_interface.export_to_serial(
                self.layout_scroller.layouts)

            if not success:
                tkMessageBox.showerror("Serial Export Error",
                                       error_string)

        self.exportMenu.add_command(label="Export Serial",
                                    command=export_serial_callback,
                                    underline=7)

        # 3 Separator
        self.exportMenu.add_separator()

        # 4 Add an option to to reset the board back to its default
        #   display pattern
        def clear_nvmem_callback():
            # Tell the microcontroller to clear the pattern in its memory
            success, error_string = in_pc_interface.serial_clear_nv_memory()

            if not success:
                tkMessageBox.showerror("Serial Export Error",
                                       error_string)
        self.exportMenu.add_command(label="Clear Board Memory",
                                    command=clear_nvmem_callback,
                                    underline=1)

        # Disable the Export to Serial command if the user specified a bad
        # port or disabled this functionality
        self.set_serial_menu_items(use_serial)

        # Create and populate the play menu
        self.playMenu = Tkinter.Menu(self.menu)
        self.menu.add_cascade(label="Playback",
                              menu=self.playMenu,
                              underline=0)

        # Add option to loop play from beginning
        self.playMenu.add_command(label="Play from Beginning",
                                  command=self._play_from_start_handler,
                                  underline=10)

        # Add option to play from current slide
        self.playMenu.add_command(label="Play from Current Slide",
                                  command=self._play_from_current_handler,
                                  underline=10)

        # Add option to stop playback
        self.playMenu.add_command(label="Stop Playback",
                                  command=self.stop_playback, underline=3,
                                  state=Tkinter.DISABLED)

    def set_serial_menu_items(self, in_state):
        """Sets the menu items to the given state

        Args:
            in_state: Set the items to NORMAL?
        """
        if in_state:
            in_state = Tkinter.NORMAL
        else:
            in_state = Tkinter.DISABLED
        self.exportMenu.entryconfig(EXPORT_SERIAL_LOC, state=in_state)
        self.exportMenu.entryconfig(CLEAR_MEM_LOC, state=in_state)

    def _play_from_start_handler(self):
        """Handles the user clicking the menu item for playing all of the
        screens from the start of the pattern."""

        def real_handler():
            self.play_screens(0)

        return real_handler()

    def _play_from_current_handler(self):
        """Handles the user clicking the menu item for playing all of the
        screens from currently selected screen."""
        def real_handler():
            self.play_screens(self.layout_scroller.selectedScreen)

        return real_handler()

    def prompt_user_to_save(self):
        """Prompts the user to save the current layout if changes have
        been made to the file since it was last saved.

        Returns:
          A boolean indicating if the user successfully answered the
            question, e.g. The user did not press cancel.
        """

        return_value = False

        if self.noChangesAfterSave:
            return_value = True
        else:
            response = tkMessageBox.askquestion('Save Current?',
                                                'Do you want to save the '
                                                'current layout?',
                                                type=tkMessageBox.YESNOCANCEL)
            if response != tkMessageBox.CANCEL:
                if response == tkMessageBox.YES:
                    if self.save_or_save_as():
                        return_value = True
                else:
                    return_value = True

        return return_value

    def save_or_save_as(self):
        """Determines whether to do a save or ask the user for a file name.

        If there is not current file name the user will be prompted to
        choose a file name.

        Returns:
          The name of the file to save as.
        """
        if self.saveFileName != '':
            return_value = self.save_file(self.saveFileName)
        else:
            return_value = self.save_file_as()

        return return_value

    def new_file_handler(self):
        """Handles a menu request to create a new sequence."""

        def real_handler():
            self.new_sequence()
        return real_handler

    def save_file_handler(self, saveLocation):
        """Handles a menu request to save the current sequence.

        Args:
            saveLocation: The location to save the file at.
        """

        def real_handler():
            self.save_or_save_as()
        return real_handler

    def save_file_as_handler(self):
        """Handles a menu request to save the current sequence as a
            new file."""

        def real_handler():
            self.save_file_as()
        return real_handler

    def new_sequence(self):
        """Removes all existing screens.

        Prior to removing the old screens, the user is asked if he wants
        to save the old sequence.
        """
        if self.prompt_user_to_save():
            self.layout_scroller.remove_all_screens()
            self.noChangesAfterSave = True
            self.saveFileName = ''
            self.root.title(os.path.basename(self.saveFileName) + ' - ' + DEFAULT_TITLE)

    def open_file_handler(self):
        """Handles a menu request to open a sequence file."""

        def real_handler():
            if self.prompt_user_to_save():
                self.open_file_dialog()
        return real_handler

    def save_file(self, save_location):
        """Handles the saving of a file.

        This function pickles the screens of the classes layout scroller
        and saves the file to the given location. Delays are converted from
        type StringVar to plain str values.

        Args:
            save_location: The file where the sequence is to be saved to.
        """
        if save_location != '':
            # Commit the current page to the screen array
            self.layout_scroller.store_current_screen()

            # Convert the delay string vars to string before pickling.
            # Fails otherwise. Duplicates the array otherwise delays
            # StringVars are overwritten with basic strings
            out_screen_array = []
            for i in range(0, self.layout_scroller.layouts.length()):
                out_screen_array.append(
                    self.layout_scroller.layouts.get_screen(i))

            f = open(save_location, 'w')

            pickle.dump(out_screen_array, f)
            f.close()
            self.noChangesAfterSave = True
            return_value = True
            self.root.title(os.path.basename(save_location) + ' - ' + DEFAULT_TITLE)

        else:
            return_value = False

        return return_value

    def save_file_as(self):
        """Presents the user with a Save As dialog.

        Returns:
            Boolean indicating whether the user successfully chose a
            filename to save as.
        """
        self.saveFileName = asksaveasfilename(
            title='Save LightBoard Session As...',
            filetypes=self.fileTypeTuple,
            defaultextension=self.defaultExtension)

        if self.saveFileName != '':
            self.save_file(self.saveFileName)
            return_value = True
        else:
            return_value = False

        return return_value

    def open_file_dialog(self):
        """Presents the user with an Open File Dialog and opens the file."""
        file_name = askopenfilename(
            title='Open LightBoard Session...',
            filetypes=self.fileTypeTuple)

        if len(file_name) > 0:
            self.open_this_file(file_name)

    def open_this_file(self, open_file_name):
        """Opens the given file and displays its contents.

        The given file is unpickled and loaded into the layouts in
        the layout_scroller. The delays are convert from plain str
        variables to the type IntVar. The first screen is selected
        and displayed. The program's root windows is retitled to include
        the active file.

        Args:
            open_file_name: The file to open that contains a pickled array
                of screens

        Raises:
            IOError: An error occurred when opening or closing the file.
        """

        board_layout = buttGrid_Filer.FileManager.load_file(open_file_name)

        if board_layout is None:
            # print('No such file: %s' % open_file_name)
            return

        # Remove all of the existing screens and populate with the new screens
        self.layout_scroller.remove_all_screens()

        assert isinstance(board_layout, buttGrid_Layout.BoardLayout)
        for index in range(board_layout.length()):
            pattern, delay = board_layout.get_screen(index)

            self.layout_scroller.insert_new_screen(place_to_insert=index,
                                                   pattern=pattern,
                                                   delay=delay,
                                                   focus_on_new=False)
            index += 1

        # After we're done inserting the original blank screen will still exist
        self.layout_scroller.remove_screen(board_layout.length())

        # By using previous_was_deleted, the current display (blank) will not
        # overwrite the current screen (0)
        self.layout_scroller.select_screen(0, previous_was_deleted=True)

        self.noChangesAfterSave = True
        self.saveFileName = open_file_name
        self.root.title(os.path.basename(self.saveFileName) + ' - ' + DEFAULT_TITLE)

    def play_screens(self, start_slide=0):
        """Play screens at their set speed starting from the given start_slide

        Args:
            start_slide: The first slide to play. Defaults to the first slide
        """
        if start_slide < 0:
            start_slide = 0
        elif start_slide >= self.layout_scroller.layouts.length():
            start_slide = self.layout_scroller.layouts.length() - 1

        initial_screen = self.layout_scroller.selectedScreen

        self.playMenu.entryconfig(STOP_PLAY_LOC, state=Tkinter.NORMAL)
        self.playMenu.entryconfig(PLAY_START_LOC, state=Tkinter.DISABLED)
        self.playMenu.entryconfig(PLAY_HERE_LOC, state=Tkinter.DISABLED)
        self.buttonGrid.set_button_state(Tkinter.DISABLED)
        self.layout_scroller.set_delay_box_state(Tkinter.DISABLED)

        delay_duration = self.layout_scroller.layouts.get_delay(start_slide)
        self.layout_scroller.select_screen(start_slide)

        def handler():
                self.playing_screens(start_slide, initial_screen)

        self.idPlayback = self.after(10 * delay_duration, handler)

    def playing_screens(self, active_screen, initial_screen):
        """Plays the next screen in the order and delays the given time

        If the current screen is the last one in the pattern, display the
        initial screen and exit

        Args:
            active_screen: The currently displayed screen number

            initial_screen: The screen to be display once all of the others
            have been
        """
        active_screen += 1
        if active_screen < self.layout_scroller.layouts.length():
            delay_duration = self.layout_scroller.layouts.get_delay(
                active_screen)
            self.layout_scroller.select_screen(active_screen)

            def handler():
                self.playing_screens(active_screen, initial_screen)

            self.idPlayback = self.after(10 * delay_duration, handler)
        else:
            self.layout_scroller.select_screen(initial_screen)
            self.return_from_playback()

    def stop_playback(self):
        """Stops the playback of the screens because the user commanded it
        """
        self.after_cancel(self.idPlayback)
        self.return_from_playback()

    def return_from_playback(self):
        """Return inputs back to pre-playback states

        Re-enables the button grid and resets the playback menu options
        """
        self.playMenu.entryconfig(STOP_PLAY_LOC, state=Tkinter.DISABLED)
        self.playMenu.entryconfig(PLAY_START_LOC, state=Tkinter.NORMAL)
        self.playMenu.entryconfig(PLAY_HERE_LOC, state=Tkinter.NORMAL)
        self.buttonGrid.set_button_state(Tkinter.NORMAL)
        self.layout_scroller.set_delay_box_state(Tkinter.NORMAL)


class MainFrame(object):
    """Creates the grid of buttons and binds the key combinations related
    to manipulating the grid.

    Attributes:
        buttonGrid: Local instance of the grid of buttons used to display
            the light grid.
        layout_scroller: Local instance of the class containing all of the
            display screens.
        menuBar: Local instance of the class containing the menu bar where
            the file operation function are contained.
    """

    # ROW_SET_ROW = 1  # Row to put row set buttons on
    # ROW_SET_ROW_SPAN = 2  # Number of rows to consume with row set buttons
    # ROW_SET_COL = 1  # Column to start row set buttons on
    # ROW_SET_COL_SPAN = 1  # Column consumed by row set buttons

    GRID_ROW = 1  # Row to put pattern grid on
    GRID_ROW_SPAN = 2  # Rows consumed by the pattern grid
    GRID_COL = 1  # Column to start pattern grid on
    GRID_COL_SPAN = 5  # Columns consumed by pattern grid

    SHIFT_UP_ROW = 0  # Row to put the upward shift buttons
    SHIFT_UP_COL = 1  # Column to start the upward shift buttons
    SHIFT_UP_COL_SPAN = 2  # Number of columns each up shift button is to use

    SHIFT_RIGHT_ROW = 1   # Row to start the rightward shift buttons
    SHIFT_RIGHT_COL = 6  # Column to place the rightward shift buttons

    SHIFT_DOWN_ROW = GRID_ROW + GRID_ROW_SPAN  # Row for downward shift buttons
    SHIFT_DOWN_COL = 1  # Column to start the downward  shift buttons
    SHIFT_DOWN_COL_SPAN = 2  # Number of columns each down shift button uses

    SHIFT_LEFT_ROW = 1  # Row to start the leftward shift buttons
    SHIFT_LEFT_COL = 0  # Column to place the leftward shift buttons

    OPTION_ROW = SHIFT_DOWN_ROW + 1  # Row for layout option buttons
    OPTION_ROW_SPAN = 1  # Number of rows to occupy with layout option buttons
    OPTION_COLUMN = GRID_COL  # Column to start layout option buttons on
    OPTION_BUTTON_WIDTH = 8  # Size of the option buttons
    OPTION_BUTTON_PAD_X = 1  # Padding between option buttons

    LAYOUT_ROW = OPTION_ROW + OPTION_ROW_SPAN  # Row to put layout scroller on
    LAYOUT_COL = 1  # Column to start layout scroller on
    LAYOUT_COL_SPAN = 5  # Columns consumed by layout scroller

    def __init__(self, root, settings, use_serial, micro_interface):
        """Draws the grid and binds the grid manipulation key combinations.

        Args:
            self: The instance of this class.
            root: The Tk object that this grid will be displayed on.
            settings: Settings object containing the settings for the program
            use_serial: True if the serial port should be used for
                        communicating with the display
            micro_interface: Interface with the microcontroller
        """

        # Initialize the settings window
        settings_menu = buttGrid_Settings.SettingsMenu(root,
                                                       micro_interface,
                                                       settings)

        grid_frame = Tkinter.Frame(root)
        # Add row setter buttons
        row_setter_frame = Tkinter.Frame(grid_frame)

        for row in range(buttGrid_Grid.ButtonGrid.NBR_OF_ROWS):
            def row_set_callback(index=row):
                if self.button_grid is not None:
                    self.button_grid.toggle_row_state(index)

            a_row_button = Tkinter.Button(row_setter_frame,
                                          text=row,
                                          padx=0,
                                          pady=0,
                                          command=row_set_callback)
            a_row_button.grid(row=row,
                              column=0,
                              sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

            row_setter_frame.rowconfigure(row, weight=1)

        row_setter_frame.grid(row=1,
                              column=0,
                              rowspan=1,
                              columnspan=1,
                              sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Add column setter buttons
        column_setter_frame = Tkinter.Frame(grid_frame)

        for column in range(buttGrid_Grid.ButtonGrid.NBR_OF_COLUMNS):
            def column_set_callback(index=column):
                if self.button_grid is not None:
                    self.button_grid.toggle_column_state(index)

            a_row_button = Tkinter.Button(column_setter_frame,
                                          text=column,
                                          padx=0,
                                          pady=0,
                                          command=column_set_callback)
            a_row_button.grid(row=0,
                              column=column,
                              sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

            column_setter_frame.columnconfigure(column, weight=1)

        column_setter_frame.grid(row=0,
                                 column=1,
                                 rowspan=1,
                                 columnspan=1,
                                 sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W
                                 )

        # Initialize the button grid
        self.button_grid = buttGrid_Grid.ButtonGrid(grid_frame)

        # Add the button grid to the grid frame
        self.button_grid.grid(row=1,
                              column=1,
                              columnspan=1,
                              rowspan=1,
                              sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Allow the grid to stretch with the window
        grid_frame.rowconfigure(1,
                                weight=1)

        grid_frame.columnconfigure(1,
                                   weight=1)

        # Add the frame with the grid and row setters to the root grid
        grid_frame.grid(row=self.GRID_ROW,
                        column=self.GRID_COL,
                        rowspan=self.GRID_ROW_SPAN,
                        columnspan=self.GRID_COL_SPAN,
                        sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Set up the layout scroller and attach it to the other parts of
        # the program
        self.layout_scroller = buttGrid_Grid.LayoutScroller(self.button_grid,
                                                            settings)
        self.button_grid.set_layout_scroller(self.layout_scroller)

        # Set the menu bar up and attach it to the other parts of the program
        self.menu_bar = MenuBar(
            root=root,
            in_button_grid=self.button_grid,
            in_layout_scroller=self.layout_scroller,
            in_pc_interface=micro_interface,
            use_serial=use_serial,
            in_settings=settings_menu)

        self.button_grid.set_menu_bar(self.menu_bar)
        self.layout_scroller.set_menu_bar(self.menu_bar)

        # Append the file name to the window title, if a name exists
        if len(self.menu_bar.saveFileName) > 0:
            root.title(os.path.basename(self.menu_bar.saveFileName) + ' - ' + DEFAULT_TITLE)
        else:
            root.title(DEFAULT_TITLE)
            
        # Allow all columns and rows surrounding the button grid to stretch
        # with the window
        for row in range(0, self.GRID_ROW_SPAN):
            Tkinter.Grid.rowconfigure(root,
                                      self.GRID_ROW + row,
                                      weight=1)

        for col in range(0, self.GRID_COL_SPAN):
            Tkinter.Grid.columnconfigure(root,
                                         self.GRID_COL + col,
                                         weight=1)

        self.layout_scroller.grid(row=self.LAYOUT_ROW,
                                  column=self.LAYOUT_COL,
                                  columnspan=self.LAYOUT_COL_SPAN,
                                  sticky=Tkinter.E+Tkinter.W)

        # Add the screen modification buttons to the window
        self.setup_screen_modification_buttons(root)

        # Shift buttons
        self.setup_shift_buttons(root)

        # Bind the button grid modification key combos
        root.bind("<Control-Key-c>", self._ctl_c_bind)  # Clear grid
        root.bind("<Control-Key-a>", self._ctl_a_bind)  # Add new screen
        root.bind("<Control-Key-r>", self._ctl_r_bind)  # Remove screen
        root.bind("<Control-Key-i>", self._ctl_i_bind)  # Invert screen
        root.bind("<Control-Key-d>", self._ctl_d_bind)  # Duplicate screen

        # Bind the file function key combos
        root.bind("<Control-Key-s>", self._ctl_s_bind)  # Save program
        root.bind("<Control-Key-n>", self._ctl_n_bind)  # New program
        root.bind("<Control-Key-o>", self._ctl_o_bind)  # Open program

        # Bind the button grid navigation to the arrow keys
        root.bind("<Left>", self._left_arrow_bind)
        root.bind("<Right>", self._right_arrow_bind)

        # Bind the playback keys
        root.bind("<Control-Key-p>", self._ctl_p_bind)  # Play from start
        root.bind("<Control-Key-P>", self._ctl_shift_p_bind)  # Play from current
        root.bind("<Control-Key-t>", self._ctl_t_bind)  # Stop playback

        self.layout_scroller.define_remove_button(self.remove_button)

    # Key combos for the button grid
    def _ctl_c_bind(self, event):
        """Key binding handler for Ctl-c, used to clear the current grid."""
        self.button_grid.clear_grid()

    def _ctl_a_bind(self, event):
        """Key binding handler for Ctl-a, used to insert a blank grid."""
        self.layout_scroller.insert_new_screen()

    def _ctl_r_bind(self, event):
        """Key binding handler for Ctl-r, used to remove the current grid."""
        if self.remove_button['state'] != Tkinter.DISABLED:
            self.layout_scroller.remove_screen()

    def _ctl_i_bind(self, event):
        """Key binding handler for Ctl-i, used to invert the current grid."""
        self.button_grid.invert_button_states()

    def _ctl_d_bind(self, event):
        """Key binding handler for Ctl-d, used to duplicate the current
        grid."""
        self.layout_scroller.duplicate_screen()

    # File function key combos
    def _ctl_s_bind(self, event):
        """Key binding handler for Ctl-s, used to save the current sequence."""
        self.menu_bar.save_or_save_as()

    def _ctl_n_bind(self, event):
        """Key binding handler for Ctl-n, used to create a new sequence."""
        self.menu_bar.new_sequence()

    def _ctl_o_bind(self, event):
        """Key binding handler for Ctl-o, used to open another sequence."""
        self.menu_bar.open_file_dialog()

    # Button grid navigation arrow key combos
    def _left_arrow_bind(self, event):
        """Key binding handler for the left arrow,
        used to move to the next grid."""
        if self.layout_scroller.selectedScreen > 0:
            self.layout_scroller.select_screen(
                self.layout_scroller.selectedScreen - 1)

    def _right_arrow_bind(self, event):
        """Key binding handler for the left arrow,
        used to move to the previous grid."""
        if self.layout_scroller.selectedScreen < \
                (self.layout_scroller.layouts.length() - 1):

            self.layout_scroller.select_screen(
                self.layout_scroller.selectedScreen + 1)

    # Playback key combos
    def _ctl_p_bind(self, event):
        """Keybinding to start playback from the beginning"""
        self.menu_bar.play_screens(0)

    def _ctl_shift_p_bind(self, event):
        """Keybinding to start playback from the current screen"""
        self.menu_bar.play_screens(self.layout_scroller.selectedScreen)

    def _ctl_t_bind(self, event):
        """Keybinding to stop playback"""
        self.menu_bar.stop_playback()

    def setup_screen_modification_buttons(self, root):
        """ Adds the screen modification buttons
        :param root: Root window to add the buttons to
        :return:
        """
        clear_button = Tkinter.Button(root,
                                      text="Clear",
                                      padx=self.OPTION_BUTTON_PAD_X,
                                      pady=0,
                                      width=self.OPTION_BUTTON_WIDTH,
                                      command=self.button_grid.clear_grid,
                                      underline=0)
        clear_button.grid(row=self.OPTION_ROW,
                          column=self.OPTION_COLUMN)

        add_button = Tkinter.Button(root,
                                    text="Add",
                                    padx=self.OPTION_BUTTON_PAD_X,
                                    pady=0,
                                    width=self.OPTION_BUTTON_WIDTH,
                                    command=
                                    self.layout_scroller.insert_new_screen,
                                    underline=0)
        add_button.grid(row=self.OPTION_ROW,
                        column=self.OPTION_COLUMN + 1)

        self.remove_button = Tkinter.Button(root,
                                            text="Remove",
                                            padx=self.OPTION_BUTTON_PAD_X,
                                            pady=0,
                                            width=self.OPTION_BUTTON_WIDTH,
                                            command=
                                            self.layout_scroller.remove_screen,
                                            state=Tkinter.DISABLED,
                                            underline=0)
        self.remove_button.grid(row=self.OPTION_ROW,
                                column=self.OPTION_COLUMN + 2)

        invert_button = Tkinter.Button(root,
                                       text="Invert",
                                       padx=self.OPTION_BUTTON_PAD_X,
                                       pady=0,
                                       width=self.OPTION_BUTTON_WIDTH,
                                       command=
                                       self.button_grid.invert_button_states,
                                       underline=0)
        invert_button.grid(row=self.OPTION_ROW,
                           column=self.OPTION_COLUMN + 3)

        duplicate_button = Tkinter.Button(root,
                                          text="Duplicate",
                                          padx=self.OPTION_BUTTON_PAD_X,
                                          pady=0,
                                          width=self.OPTION_BUTTON_WIDTH,
                                          command=
                                          self.layout_scroller.duplicate_screen,
                                          underline=0)
        duplicate_button.grid(row=self.OPTION_ROW,
                              column=self.OPTION_COLUMN + 4)

    def setup_shift_buttons(self, root):
        """ Adds the shift screen buttons to the root window

        :param root: Root window to add the buttons to
        :return: None
        """
        # Left
        def callback_left_shift():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_LEFT,
                                               rotate=False)
        left_shift_button = Tkinter.Button(root,
                                           text="SL",
                                           padx=0,
                                           pady=0,
                                           underline=0,
                                           command=callback_left_shift)
        left_shift_button.grid(row=self.SHIFT_LEFT_ROW,
                               column=self.SHIFT_LEFT_COL,
                               sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        def callback_left_rotate():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_LEFT,
                                               rotate=True)
        left_rotate_button = Tkinter.Button(root,
                                            text="RoL",
                                            padx=0,
                                            pady=0,
                                            underline=0,
                                            command=callback_left_rotate)
        left_rotate_button.grid(row=self.SHIFT_LEFT_ROW + 1,
                                column=self.SHIFT_LEFT_COL,
                                sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Up
        def callback_up_shift():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_UP,
                                               rotate=False)
        up_shift_button = Tkinter.Button(root,
                                         text="SU",
                                         padx=0,
                                         pady=0,
                                         underline=0,
                                         command=callback_up_shift)
        up_shift_button.grid(row=self.SHIFT_UP_ROW,
                             column=self.SHIFT_UP_COL,
                             columnspan=self.SHIFT_UP_COL_SPAN,
                             sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        def callback_up_rotate():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_UP,
                                               rotate=True)
        up_rotate_button = Tkinter.Button(root,
                                          text="RoU",
                                          padx=0,
                                          pady=0,
                                          underline=0,
                                          command=callback_up_rotate)
        up_rotate_button.grid(row=self.SHIFT_UP_ROW,
                              column=self.SHIFT_RIGHT_COL-self.SHIFT_UP_COL_SPAN,
                              columnspan=self.SHIFT_UP_COL_SPAN,
                              sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Right
        def callback_right_shift():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_RIGHT,
                                               rotate=False)
        right_shift_button = Tkinter.Button(root,
                                            text="SR",
                                            padx=0,
                                            pady=0,
                                            underline=0,
                                            command=callback_right_shift)
        right_shift_button.grid(row=self.SHIFT_RIGHT_ROW,
                                column=self.SHIFT_RIGHT_COL,
                                sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        def callback_right_rotate():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_RIGHT,
                                               rotate=True)
        right_rotate_button = Tkinter.Button(root,
                                             text="RoR",
                                             padx=0,
                                             pady=0,
                                             underline=0,
                                             command=callback_right_rotate)
        right_rotate_button.grid(row=self.SHIFT_RIGHT_ROW + 1,
                                 column=self.SHIFT_RIGHT_COL,
                                 sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        # Down
        def callback_down_shift():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_DOWN,
                                               rotate=False)
        down_shift_button = Tkinter.Button(root,
                                           text="SD",
                                           padx=0,
                                           pady=0,
                                           underline=0,
                                           command=callback_down_shift)
        down_shift_button.grid(row=self.SHIFT_DOWN_ROW,
                               column=self.SHIFT_DOWN_COL,
                               columnspan=self.SHIFT_UP_COL_SPAN,
                               sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)

        def callback_down_rotate():
            self.layout_scroller.shift_pattern(self.layout_scroller.SHIFT_DOWN,
                                               rotate=True)
        down_rotate_button = Tkinter.Button(root,
                                            text="RoD",
                                            padx=0,
                                            pady=0,
                                            underline=0,
                                            command=callback_down_rotate)
        down_rotate_button.grid(row=self.SHIFT_DOWN_ROW,
                                column=self.SHIFT_RIGHT_COL-self.SHIFT_DOWN_COL_SPAN,
                                columnspan=self.SHIFT_DOWN_COL_SPAN,
                                sticky=Tkinter.N+Tkinter.S+Tkinter.E+Tkinter.W)
